// Took this from : https://gist.github.com/stoefln/1438c5cf39b8acb94c21c5f785f69e29

const path = require('path')
const shell = require('shelljs')

const projectRoot = shell.pwd().stdout
const nativeNodeDir = path.resolve(projectRoot, 'build/Release')
const libDir = 'dist/dependancy/libDeps'
const executablesDir = 'dist/dependancy/executable'
const libDependencyDir = projectRoot + '/' + libDir
const executableDependencyDir = projectRoot + '/' + executablesDir

// Dependancy copy of exe
const libDir4Exe = 'dist/dependancy/exeDepends'
const exelibDependencyDir = projectRoot + '/' + libDir4Exe


shell.mkdir('-p', libDependencyDir)
shell.mkdir('-p', executableDependencyDir)
shell.mkdir('-p', exelibDependencyDir)

if (process.platform === 'darwin') {

  const boostLibDirMac = '/usr/local/lib/'

  //---------------------------------------------------------------------------------------------------

  // Fix all dependency of AvatarCore.node and avatar_services and copy related libs to distributable direcotry
  function fix3rdPatyDeps(dirPath, type, installNameUpdate, grepPara, nameToolFix, filePermission, copyTo) {
    
    const nodeFiles = shell.ls(dirPath)
    const allDeps = []

    nodeFiles.filter(file => {
        return -1 != file.indexOf(type)
      })
      .forEach(nodeFileName => {
        const nodeFile = dirPath + '/' + nodeFileName
        const outerDeps = shell
          // eg : otool -l ./dist/dependancy/native/AvatarCoreAPI.node | grep 'name /usr/local/\\| name /Library/Frameworks/'
          .exec(`otool -l ${nodeFile} | grep '${grepPara}'`) 
          .stdout.split('\n')
          .filter(dep => {
            return dep != ''
          })

        outerDeps.forEach(depLine => {
          var regex = /name (.+?(?=\ \(offset))/g // transform "name /usr/local/...dylib (offset 24)" -> "/usr/local/...dylib"
          var depfilePath = regex.exec(depLine)[1] // "/usr/local/...dylib"
          var depfileName = depfilePath.replace(/^.*[\\\/]/, '')

          if (!allDeps.find(e => e == depfilePath)) {
            // if not added already -> add it
            allDeps.push(depfilePath)
            shell.cp('-n', depfilePath, copyTo)
          }
          shell.chmod('-v', filePermission, nodeFile)
          if (installNameUpdate) {
            // fixCommand : install_name_tool -change /usr/loca/opt/boost/libboost_system.dylib @rpath/libboost_system.dylib ./dist/dependancy/libDeps/AvatarCoreAPI.node
            const fixCommand = `install_name_tool -change ${depfilePath} ${nameToolFix}${depfileName} ${nodeFile}` 
            shell.exec(fixCommand)
          }
        })

        console.log(`\n\n`)
      })
      

    console.info('Third Party dependencies of:' + dirPath)
    console.log(allDeps)
    return allDeps
  }

  //---------------------------------------------------------------------------------------------------

  // copy boost libs
  function copyrpathBoostLibs(dirPath, type) {
    const nodeFiles = shell.ls(dirPath)
    // For fixing all /usr/local/...
    nodeFiles.filter(file => {
        return -1 != file.indexOf(type)
      })
      .forEach(nodeFileName => {
        const nodeFile = dirPath + '/' + nodeFileName
        const outerDeps = shell
          .exec(`otool -l ${nodeFile} | grep 'name @rpath'`)
          .stdout.split('\n')
          .filter(dep => {
            return dep != ''
          })

        outerDeps.forEach(depLine => {
          var regex = /name (.+?(?=\ \(offset))/g // transform "name /usr/local/...dylib (offset 24)" -> "/usr/local/...dylib"
          var depfilePath = regex.exec(depLine)[1] // "/usr/local/...dylib"
          var depfileName = depfilePath.replace(/^.*[\\\/]/, '')

          shell.cp('-n', boostLibDirMac + depfileName, libDependencyDir)
        })
        
        console.log(`\n\n`)
      })
  }

  //---------------------------------------------------------------------------------------------------
  // Recursively fix 3rd party deps of all files where type equals to @param type 
  function fix3rdPartyRecursive(dirPath, copyTo, type, installNameUpdate, nameToolFix, filePermission) {
    
    const nodeFiles = shell.ls(dirPath)
    const allDeps = []

    // For fixing all /usr/local/...
    nodeFiles.filter(file => {
        return -1 != file.indexOf(type)
      })
      .forEach(nodeFileName => {
        const nodeFile = dirPath + '/' + nodeFileName
        const outerDeps = shell
          .exec(`otool -l ${nodeFile} | grep 'name /usr/local/'`)
          .stdout.split('\n')
          .filter(dep => {
            return dep != ''
          })

        outerDeps.forEach(depLine => {
          var regex = /name (.+?(?=\ \(offset))/g // transform "name /usr/local/...dylib (offset 24)" -> "/usr/local/...dylib"
          var depfilePath = regex.exec(depLine)[1] // "/usr/local/...dylib"
          var depfileName = depfilePath.replace(/^.*[\\\/]/, '')

          if (!allDeps.find(e => e == depfilePath)) {
            // if not added already -> add it
            allDeps.push(depfilePath)
            shell.cp('-n', depfilePath, copyTo)
          }
          shell.chmod('-v', filePermission, nodeFile)
          if (installNameUpdate) {
            const fixCommand = `install_name_tool -change ${depfilePath} ${nameToolFix}${depfileName} ${nodeFile}`
            shell.exec(fixCommand)
          }
        })

        console.log(`\n\n`)
      })
    
      
    console.info('Third Party dependencies:')
    console.log(allDeps)
    return allDeps
  }

  //---------------------------------------------------------------------------------------------------
  //---------- FIXIING AVATARCORE.NODE AND RELATED DEPNDANCIES ----------------------------------------
  //---------------------------------------------------------------------------------------------------

  shell.cp('-n', nativeNodeDir+'/AvatarCoreAPI.node', libDir)

    // Copy all /usr/local...dylibs and /Library/Frameworks/ dependencies and fix rpath of avatarcoreAPI_addon.node
    fix3rdPatyDeps(libDir,
      'node',
      true,
      'name /usr/local/\\| name /Library/Frameworks/',
      '@rpath/',
      '666',
      libDependencyDir)         

  copyrpathBoostLibs(nativeNodeDir, 'node')                                                                 // Copy all boost libs

  // Recursively copy and fix dependancies
  fix3rdPartyRecursive(libDir, libDependencyDir, 'dylib', true, '@rpath/', '666')
  fix3rdPartyRecursive(libDir, libDependencyDir, 'dylib', true, '@rpath/', '666')
  fix3rdPartyRecursive(libDir, libDependencyDir, 'dylib', true, '@rpath/', '666')
  fix3rdPartyRecursive(libDir, libDependencyDir, 'dylib', true, '@rpath/', '666')
  
  // Fix libssl relative path to libcryptp
  const fixCommand = `install_name_tool -change @rpath/lib/libcrypto.1.1.dylib  @rpath/libcrypto.1.1.dylib dist/dependancy/libDeps/libssl.1.1.dylib`
  shell.exec(fixCommand)

  //---------------------------------------------------------------------------------------------------
  //---------- FIXIING AVATAR_SERVICES AND RELATED DEPNDANCIES ----------------------------------------
  //---------------------------------------------------------------------------------------------------

  shell.cp('-nf', 'avatar_services', executablesDir)
  // Copy all /usr/local...dylibs except libssl and fix path of avatar_services
  fix3rdPatyDeps(executablesDir,
      '',
      true,
      'name /usr/local/opt/gstreamer\\|name /usr/local/opt/glib\\|name /usr/local/opt/gettext\\|name /usr/local/opt/gst-plugins',
      '@executable_path/../Frameworks/exeDeps/',
      '777',
      exelibDependencyDir)
  
  fix3rdPartyRecursive(libDir4Exe, exelibDependencyDir, 'dylib', true, '@executable_path/../Frameworks/exeDeps/', '666')
  fix3rdPartyRecursive(libDir4Exe, exelibDependencyDir, 'dylib', true, '@executable_path/../Frameworks/exeDeps/', '666')
  fix3rdPartyRecursive(libDir4Exe, exelibDependencyDir, 'dylib', true, '@executable_path/../Frameworks/exeDeps/', '666')
  fix3rdPartyRecursive(libDir4Exe, exelibDependencyDir, 'dylib', true, '@executable_path/../Frameworks/exeDeps/', '666')


  //const libTool = `otool -L ./build/Release/avatarcoreAPI_addon.node`
  //shell.exec(libTool)
  
} else if (process.platform === 'win32') {
	
	let gstDlls = [
		'libffi-7.dll',
		'libglib-2.0-0.dll',
		'libgmodule-2.0-0.dll',
		'libgobject-2.0-0.dll',
		'libgstapp-1.0-0.dll',
		'libgstaudio-1.0-0.dll',
		'libgstbase-1.0-0.dll',
		'libgstreamer-1.0-0.dll',
		'libgsttag-1.0-0.dll',
		'libgstvideo-1.0-0.dll',
		'libintl-8.dll',
		'liborc-0.4-0.dll',
		'libwinpthread-1.dll',
		'libz-1.dll'
		]
		
	function copyToNativeDependancy(array) {
		array.forEach(function(item, index, array) {
			var dllPath = shell.env["GSTREAMER_1_0_ROOT_X86_64"] + "\\bin\\" + item
			console.log(dllPath)
			shell.cp('-n', dllPath, libDependencyDir)
		})
	}
		
	copyToNativeDependancy(gstDlls)
  
}