
const path = require('path')
const { exit } = require('process')
const shell = require('shelljs')


const projectRoot = shell.pwd().stdout
const addonPath = path.resolve(projectRoot, 'build/Release/')


shell.mkdir('-p', addonPath)

var avatarCorePath=process.env.AVATARCOREAPI_DIR
const avatarCoreDir = avatarCorePath+'/bin/'

process.stdout.write(avatarCoreDir +"\n")

var targetFolder="";
if (process.platform === 'darwin'){
    targetFolder=avatarCoreDir+"x86_64_Darwin/"
}else
if (process.platform === 'win32'){
    targetFolder=avatarCoreDir+"AMD64_Windows/"
}else 
if (process.platform === 'linux'){
    var arch= shell.exec("uname -m").trim()
    targetFolder=avatarCoreDir+arch+"_Linux/"
}else{
    process.stdout.write("Couldn't identify operating system!\n");
    exit(0)
}

process.stdout.write(addonPath +"\n")
process.stdout.write(targetFolder +"\n")
shell.cp('-R',targetFolder+"AvatarCoreAPI.node", addonPath)
shell.cp('-R',targetFolder+"avatar_services*", projectRoot)


if (process.platform === 'win32'){
    shell.cp('-R',targetFolder+"*.dll", projectRoot)
}

