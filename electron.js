// main.js

"use strict";

// Import parts of electron to use
const { app, BrowserWindow, screen, ipcMain } = require("electron");
const path = require("path");
const url = require("url");
const { spawn, exec } = require("child_process");
const Store =require('electron-store') ;

// Add React extension for development
//const { default: installExtension, REACT_DEVELOPER_TOOLS } = require('electron-devtools-installer')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let mainWindowOptions = null;
let status = 0;

// Keep a reference for dev mode
let dev = false;

const config = new Store();

// Determine the mode (dev or production)
if (
  process.defaultApp ||
  /[\\/]electron-prebuilt[\\/]/.test(process.execPath) ||
  /[\\/]electron[\\/]/.test(process.execPath)
) {
  dev = true;
}


// Temporary fix for broken high-dpi scale factor on Windows (125% scaling)
// info: https://github.com/electron/electron/issues/9691
if (process.platform === "win32") {
  app.commandLine.appendSwitch("high-dpi-support", "true");
  app.commandLine.appendSwitch("force-device-scale-factor", "1");
} else if (process.platform === "linux") {
}
app.commandLine.appendSwitch("--no-sandbox");

function getBrowserWindowOptions() {
  const defaultOptions = {
      width: 1280,
      height: 768,
      show: false,
      webPreferences: {
          nodeIntegration: true
      }
  };

  // { x, y, width, height }
  const lastOptions = config.get('winBounds');

  // Get display that most closely intersects the provided bounds.
  let windowOptions = {};
  if (lastOptions) {
      const display = screen.getDisplayMatching(lastOptions);

      if (display.id === lastOptions.id) {
          // use last time options when using the same display
          windowOptions = {
              ...windowOptions,
              ...lastOptions
          };
      } else {
          // or center the window when using other display
          const workArea = display.workArea;

          // calculate window size
          const width = Math.max(Math.min(lastOptions.width, workArea.width), 360);
          const height = Math.max(Math.min(lastOptions.height, workArea.height), 240);
          const x = workArea.x + (workArea.width - width) / 2;
          const y = workArea.y + (workArea.height - height) / 2;

          windowOptions = {
              id: display.id,
              x,
              y,
              width,
              height
          };
      }
  } else {
      const display = screen.getPrimaryDisplay();
      const { x, y, width, height } = display.workArea;

      windowOptions = {
          id: display.id,
          x,
          y,
          width,
          height
      };
  }

  return Object.assign({}, defaultOptions, windowOptions);
}

var avatar_proxy_process;
function createWindow() {
  const windowOptions = getBrowserWindowOptions();
  // Create the browser window.
  mainWindow = new BrowserWindow(
    windowOptions);
  mainWindowOptions = windowOptions;
    
    /*{
    width: 1024, // width of the window
    height: 768, // height of the window
    show: false, // don't show until window is ready
    webPreferences: {
      nodeIntegration: true,
    },
  });*/

  // and load the index.html of the app.
  let indexPath;

  // Determine the correct index.html file
  // (created by webpack) to load in dev and production
  if (dev && process.argv.indexOf("--noDevServer") === -1) {
    indexPath = url.format({
      protocol: "http:",
      host: "localhost:8080",
      pathname: "index.html",
      slashes: true,
    });
  } else {
    indexPath = url.format({
      protocol: "file:",
      pathname: path.join(__dirname, "dist/dependancy/webpack", "index.html"),
      slashes: true,
    });
  }

  ipcMain.on('request-apppath', (event, arg) => {
    process.stdout.write("Sending app path\n");
    event.sender.send('mainprocess-apppath', app.getAppPath());
  })

  // Load the index.html
  mainWindow.loadURL(indexPath);

  // Don't show the app window until it is ready and loaded
  mainWindow.once("ready-to-show", () => {
    /*
        var cmd = "./avatar_proxy " + ["-local-ip", "127.0.0.1", "-cmd-port",
          "8833", "-peer-id",
          "4404", "-ca-cert",
          "./keys/ca.pem", "-client-cert", "./keys/0_cert.pem",
          "-client-key", "./keys/0_key.pem"].join(" ");
        avatar_proxy_process = exec("avatar_proxy " + ["-local-ip", "127.0.0.1", "-cmd-port",
          "8833", "-peer-id",
          "4404", "-ca-cert",
          "./keys/ca.pem", "-client-cert", "./keys/0_cert.pem",
          "-client-key", "./keys/0_key.pem"].join(" "), { detached: true });
    */
    ///avatar_proxy_process.stdout.on('data', function (data) { process.stdout.write(data); })

    mainWindow.show();

    // Open the DevTools automatically if developing
    if (dev) {
      //installExtension(REACT_DEVELOPER_TOOLS).catch(err => console.log('Error loading React DevTools: ', err))
      mainWindow.webContents.openDevTools();
    }
  });

  // Emitted when the window is closed.
  mainWindow.on("closed", function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.a

    //avatar_proxy_process.kill();
    //avatar_proxy_process = null;
    mainWindow = null;
  });

  mainWindow.on("close", function (e) {
    console.log("JS-->Close\n");
    const options = {
        id: mainWindowOptions.id,
        ...mainWindow.getBounds()
    };

    console.log(JSON.stringify(options)+"\n");
    config.set('winBounds', options);
    if (status == 0) {
      if (mainWindow) {
        //e.preventDefault();
        mainWindow.webContents.send("destroy-api");
      }
    }
    mainWindow = null;
  });

  mainWindow.onbeforeunload = function () {
    if (mainWindow) {
      //e.preventDefault();
      mainWindow.webContents.send("destroy-api");
    }
  };

  process.on("uncaughtException", function (error) {
    if (mainWindow) {
      //e.preventDefault();
      mainWindow.webContents.send("destroy-api");
    }
  });
}
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", createWindow);

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    app.quit();
  }
});
app.on("window-quit", () => {
  if (mainWindow) {
    //e.preventDefault();
    mainWindow.webContents.send("destroy-api");
  }
});

app.on("activate", () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});
