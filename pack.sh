

export CSC_IDENTITY_AUTO_DISCOVERY=false
npm run core
npm run pack-react
npm run fix-deps
npm run pack-electron
npm run dist

cp avatar_services dist/
cp Variables.json dist/
cp build/Release/AvatarCoreAPI.node dist/
cp -r  services dist/
cp -r conf dist/
cp -r keys dist/