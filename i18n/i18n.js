import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import XHR from "i18next-xhr-backend";
import { initReactI18next } from 'react-i18next';

import translationEng from "./locales/en/messages.json";
import translationJap from "./locales/jp/messages.json";

i18n
.use(XHR)
.use(LanguageDetector)
.use(initReactI18next)
.init({
    debug: false,
    lng: localStorage.getItem("lang"),
    fallbackLng:"en",
    nonExplicitWhitelist:true,
    keySeparator: false, // we do not use keys in form messages.welcome
    interpolation:{
        escapeValue:false
    },
    resources:{
        en:{
            translations: translationEng
        },
        jp:{
            translations: translationJap
        }
    },

    ns: ["translations"],

    react: {
       // wait: true,
        useSuspense: false
      }

});


export default i18n;