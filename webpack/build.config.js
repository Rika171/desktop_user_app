// webpack.build.config.js

const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const BabiliPlugin = require('babili-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const FileManagerPlugin = require('filemanager-webpack-plugin')

module.exports = {
  entry: path.join(__dirname, "../src/index.jsx"),
  module: {
    rules: [
      {
        test: /\.css$/i,
        exclude:/node_modules/,
        use: [{
          loader: MiniCssExtractPlugin.loader, options: {
            publicPath: '/assets/css'
          },
        }, 'css-loader'],
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [{ loader: 'babel-loader' }]
      },
      {
        test: /\.s[ac]ss$/i,
        use: [{
          loader: MiniCssExtractPlugin.loader, options: {
            publicPath: '/assets/css'
          },
        }, 'css-loader','sass-loader'],

      },
      {
        test:/\.css$/i,
        exclude:/node_modules/,
        loader: 'style-loader'
      },
      {
        test: /\.(jpe?g|png|gif)$/,
        use: [{ loader: 'file-loader?name=img/[name]__[hash:base64:5].[ext]' }]
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        use: [{ loader: 'file-loader?name=font/[name]__[hash:base64:5].[ext]' }]
      },
      {
        test: /\.node$/,
        loader: "native-ext-loader",
        options: {
          emit : false,
          rewritePath: "./"
        }
      },
    ]
  },
  target: 'electron-renderer',
  plugins: [
    new HtmlWebpackPlugin({ title: 'Avatar-In Desktop - Prod' }),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: 'bundle.css',
      chunkFilename: '[id].css'
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    new BabiliPlugin(),
    new FileManagerPlugin({
      onEnd: {
        delete: [
          'dist/dependancy/webpack'
         ],
        mkdir: [
          'dist/dependancy/webpack'
         ],
        move: [
          {source: 'dist/bundle.css', destination: 'dist/dependancy/webpack/bundle.css'},
          {source: 'dist/img', destination: 'dist/dependancy/webpack/img'},
          {source: 'dist/index.html', destination: 'dist/dependancy/webpack/index.html'},
          {source: 'dist/main.js', destination: 'dist/dependancy/webpack/main.js'}
        ]
      }})
  ],
  stats: {
    colors: true,
    children: false,
    chunks: false,
    modules: false
  }
}