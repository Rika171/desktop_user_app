# About

Electron-React boilerplate desktop app with AvatarCore.
For Mac, Linux and Windows.

# Requirements

## Mac

### Install

### Environment Var

```bash
export AVATARCOREAPI_DIR=<Avatarcore path>
```

For example:

```bash
export AVATARCOREAPI_DIR=/home/yamen/Dev/core_deployed/AvatarCore/
```

## Linux

### Environment Var

```bash
export AVATARCOREAPI_DIR=<Avatarcore path>
```

For example:

```bash
export AVATARCOREAPI_DIR=/home/yamen/Dev/core_deployed/AvatarCore/
```

## Windows

### Install

Install Gstreamer x86_64 for runtime and developer versions

Install boost

### Environment Var

```bash
set AVATARCOREAPI_DIR=...core_deploy\AvatarCore
```

# Build

Notes : This would install node packages and build c++ wrapper

```bash
npm i
```

# Run Avatarcore Services

Before running the services, make sure to modify: Variables.json file located in "AVATARCOREAPI_DIR/bin/[Operating System]/Variables.json"

## Mac

No need to run the individual services as they will be requested from the application

## Linux

## Windows

# Run Developer App

```bash
npm run core
npm start
```

# Packaging

The main directory for distribution is caleld 'dist/dependancy'.

## Production

### React

React production code generation
All react codes turn to single js with proper paths and stored at dist/dependancy/webpack

```bash
npm run pack-react
```

### MAC : Fix Native dependencies, avatarcoreAPI_addon.node module and avCore dylibs

```bash
npm run fix-deps
```

Fix-Deps fully automated in fixing the rpath
avcore dylibs in the root directory won't be changed, ie: won't affect in developer mode

eg: Test the difference in libavatarCoreAPI.dylib

```bash
otool -L libavatarCoreAPI.dylib
otool -L dist/dependancy/native/libavatarCoreAPI.dylib
```

### Windows : Put all dependency dlls, currently gstreamer only

GSTREAMER_1_0_ROOT_X86_64 envirnment variable must present

```bash
npm run fix-deps
```

### Electron App packaging

Package Electron (react production, native node module, node_modules, dynamic libs, etc...)
First must turn off signing

```bash
export CSC_IDENTITY_AUTO_DISCOVERY=false
npm run pack-electron
```

### Distribution Dmg and zip creation

```bash
npm run dist
```

TODO : App signing, reduce file size, etc...
