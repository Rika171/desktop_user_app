import React from "react";

var coreAPI = require("./util/coreAPI");
const { ipcRenderer  } = require("electron");

class App extends React.Component {
  constructor(props) {
    super(props);

    //register some callbacks from Core
    coreAPI.AddCallbackFunction(
      coreAPI.DefaultCallbacks.InitCallbackType,
      function () {
        process.stdout.write(
          "------> Core API Init Callback function called\n"
        );
      }
    );
    coreAPI.AddCallbackFunction(
      coreAPI.DefaultCallbacks.DestroyCallbackType,
      function () {
        process.stdout.write(
          "------> Core API Destroy Callback function called\n"
        );
      }
    );
     
    ipcRenderer.on("mainprocess-apppath",(event, arg) => {
      //once we received the callback from application main process with the app path, init core
      this.InitializeCore(arg)
    })

    //request application path, so we can set it before we start AvatarCore
    ipcRenderer.send('request-apppath', "");

    ipcRenderer.on("destroy-api", function (event, store) {
      process.stdout.write("Destroy called\n");
      coreAPI.DestroyAPI();
      coreAPI.DestroyCloud();
      process.stdout.write("Destroy done\n");
    });
  }

  componentDidMount() {}

  componentWillUnmount() {
    //   alert("Destroy called")
    coreAPI.DestroyAPI();
  }

  InitializeCore(ApplicationPath)
  {

    if (process.platform === 'darwin'){
      var searchKey="app/Contents/"
      var index=ApplicationPath.indexOf(searchKey)
      if(index>0){
        ApplicationPath=ApplicationPath.slice(0,index)+searchKey+"MacOS/"
      }
    }else 
    if (process.platform === 'win32'){
      var searchKey="resources\\"
      var index=ApplicationPath.indexOf(searchKey)
      if(index>0){
        ApplicationPath=ApplicationPath.slice(0,index)
      }
    }

    coreAPI.InitAPI(true,ApplicationPath);
  }

  render() {
    return <div>{this.props.children}</div>;
  }
}

export { App };
