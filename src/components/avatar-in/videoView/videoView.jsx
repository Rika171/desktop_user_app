import React from "react";
import { Typography, Grid, withStyles, Button } from "@material-ui/core";
import webGlVideoRenderer from "webgl-video-renderer";
import styles from "./videoView.styles.js";
var coreAPI = require("../../../util/coreAPI");

class Video extends React.Component {
  constructor(props) {
    super(props);
    this.resource = props.resource;
    this.canvasRef = React.createRef();
    this.startVideo = this.startVideo.bind(this);
    this.stopVideo = this.stopVideo.bind(this);
    this.renderFrame = this.renderFrame.bind(this);
    this.avatarCoreVideoWrapper = null;
    //
    this.targetBlur = 0;
    this.targetGrayscale = 0;
    this.currBlur = 0;
    this.currGrayscale = 0;
    this.state = { fps: "", blur: "0px", grayscale: "0%" };
    this.prevState = { fps: "", blur: "0px", grayscale: "0%" };
  }

  componentDidMount() {
    this.renderContext = webGlVideoRenderer.setupCanvas(this.canvasRef.current);
    this.renderContext.fillBlack();
    this.startVideo(this.props.resource);
  }

  componentWillUnmount() {
    this.stopVideo();
  }

  render() {
    const { classes, t, title } = this.props;

    return (
      <React.Fragment>
        <Grid container className={classes.body_layout}>
          <Grid item container xs={12} justify="center" alignItems="center">
            <Grid item xs={12} className={classes.video_container}>
              <div style={{ filter: "blur(" + this.state.blur + ")" }}>
                <div
                  style={{ filter: "grayscale(" + this.state.grayscale + ")" }}
                >
                  <canvas
                    ref={this.canvasRef}
                    className={classes.canvas}
                  ></canvas>
                </div>
              </div>
            </Grid>
          </Grid>
          <Grid item container xs={12} justify="center" alignItems="center">
            <Typography component="h6" variant="h6" className={classes.title}>
              {title}
            </Typography>
            <Typography component="h6" variant="h6" className={classes.title}>
              (FPS: {this.state.fps})
            </Typography>
          </Grid>
          <Grid container spacing={2} className={classes.form}>
            <Grid item xs={12} sm={6}>
              <Button onClick={this.enableCamera} variant="contained" fullWidth>
                {t("mStartVideo")}
              </Button>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Button
                onClick={this.disableCamera}
                variant="contained"
                fullWidth
              >
                {t("mStopVideo")}
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }

  updateFunction() {
    var dt = Date.now() - this.lastFrame;

    if (dt > 1000) {
      //    this.clearView();
      this.targetBlur = 40;
      this.targetGrayscale = 100;
    } else {
      this.targetBlur = 0;
      this.targetGrayscale = 0;
    }

    this.currBlur += (this.targetBlur - this.currBlur) * 0.03;
    this.currGrayscale += (this.targetGrayscale - this.currGrayscale) * 0.03;

    var s = {
      blur: Math.floor(this.currBlur).toString() + "px",
      grayscale: Math.floor(this.currGrayscale).toString() + "%",
      fps: this.avatarCoreVideoWrapper.getAttribute("fps"),
    };
    return;
    if (
      s.blur != this.prevState.blur ||
      s.grayscale != this.prevState.grayscale ||
      s.fps != this.prevState.fps
    ) {
      this.setState(s);
      this.prevState = s;
    }
  }

  renderFrame(frame) {
    // console.log('callback:', frame.width, frame.height, frame.uOffset, frame.vOffset);
    this.lastFrame = Date.now();
    this.renderContext.render(
      frame,
      frame.width,
      frame.height,
      frame.uOffset,
      frame.vOffset
    );
  }

  clearView() {
    var gl = this.renderContext.gl;

    gl.clearColor(0, 0, 0.0, 0.3);
    gl.clear(gl.COLOR_BUFFER_BIT);
  }

  enableCamera() {
    coreAPI.PublishMessage("", "service/start", "Capture/video/front");
  }
  disableCamera() {
    coreAPI.PublishMessage("", "service/stop", "Capture/video/front");
  }

  // Other
  startVideo() {
    if (this.avatarCoreVideoWrapper === null) {
      this.avatarCoreVideoWrapper = coreAPI.CreateVideoWrapper();

      this.timeout = setInterval(this.updateFunction.bind(this), 10);
      this.lastFrame = Date.now();
      // console.log(this.resource);

      this.avatarCoreVideoWrapper.startStreaming(
        this.renderFrame,
        this.resource,
        10
      );
    }
  }

  stopVideo() {
    if (this.avatarCoreVideoWrapper !== null) {
      clearInterval(this.timeout);
      // console.log(this.resource + " Closing");
      this.clearView();
      this.avatarCoreVideoWrapper.stopStreaming(function () {});
      this.avatarCoreVideoWrapper.destroy();
      this.avatarCoreVideoWrapper = null; // This would help gc to regain memory and c++ object destructor
    }
  }
}

export default withStyles(styles)(Video);
