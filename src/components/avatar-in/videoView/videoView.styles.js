import variables from "../../../styles/variables.scss";

export default theme => ({
    body_layout: {
        margin: theme.spacing(1),
    },
    title: {
        fontWeight: 'bold',
        
    },
    body_main: {
        justifyContent: "center",
        width: "100vw",
        bottom: "100vh"
    },
    form: {
        width: "100%",
        justifyContent: "center",
        justify: "center",
        alignItems: "center",
        display: 'flex',
        flexDirection: 'row',
        display: "flex"
    },
    video_container: {
        width: "100%",
        justifyContent: "center",
        justify: "center",
        alignItems: "center",
    },
    canvas: {
        width: "calc(100% - 16px)",
        aspectRatio: 16/9,
    }
});