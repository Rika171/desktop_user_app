import React from "react";
import { Typography, Grid, withStyles, Button } from "@material-ui/core";
var coreAPI = require("../../../util/coreAPI");
import moustTrap from "mousetrap";
import styles from "./dataView.styles.js";
import { relativeTimeRounding } from "moment";

class Data extends React.Component {
  constructor(props) {
    super(props);
    this.start = this.start.bind(this);
    this.stop = this.stop.bind(this);
    this.avatarCoreDataWrapper = coreAPI.CreateDataWrapper();
    this.state = { data: "" };
    this.keyState = {
      ArrowUp: false,
      ArrowDown: false,
      ArrowLeft: false,
      ArrowRight: false,
    };
  }

  sendKey(type, key) {
    // console.log(key);
    this.keyState[key.key] = key.type == "keydown" ? true : false;
    var speed =
      900 *
      ((this.keyState["ArrowUp"] ? 1 : 0) -
        (this.keyState["ArrowDown"] ? 1 : 0));
    var rot =
      400 *
      ((this.keyState["ArrowRight"] ? 1 : 0) -
        (this.keyState["ArrowLeft"] ? 1 : 0));
    this.avatarCoreDataWrapper.sendData(
      "robot",
      JSON.stringify({ type: "setSpeed", speed: speed, rot: rot })
    );
  }
  componentDidMount() {
    this.start(this.props.resource, this.props.channel);
    if (this.props.allowInput == "true") {
      moustTrap.bind("up", (key) => this.sendKey("Forward", key), "keydown");
      moustTrap.bind("down", (key) => this.sendKey("Backword", key), "keydown");
      moustTrap.bind(
        "left",
        (key) => this.sendKey("Rotate Left", key),
        "keydown"
      );
      moustTrap.bind(
        "right",
        (key) => this.sendKey("Rotate Right", key),
        "keydown"
      );
      moustTrap.bind(
        ["up", "down", "left", "right"],
        (key) => this.sendKey("Stop", key),
        "keyup"
      );

      moustTrap.bind("command+up", function () {
        console.log("Forward Fast");
      });
    }
  }

  componentWillUnmount() {
    this.stop();
    this.avatarCoreDataWrapper.destroy();
    this.avatarCoreDataWrapper = null;
  }

  render() {
    return (
      <React.Fragment>
        <div>
          <h1>
            Data@ {this.props.resource}:{this.props.channel}
          </h1>
          <h2>{this.state.data}.</h2>
        </div>
      </React.Fragment>
    );
  }

  onDataArrived(data) {
    var msg = "";
    var keys = JSON.parse(data);
    if ("type" in keys) {
      switch (keys["type"]) {
        case "setSpeed": //received from user, controls robot speed
          coreAPI.PublishMessage(
            "",
            "botshell/motionDir",
            keys["speed"] + "," + keys["rot"]
          );
          msg = keys["speed"] + "," + keys["rot"];
          break;
        case "lidar": //received from robot, carries lidar data
          {
            this.setState({ lidarPoints: keys["lidar"] });
          }
          break;
        case "message":
          msg = keys["message"];
          break;
      }
    }
    if (this.props.onNewMessage !== undefined) {
      this.props.onNewMessage(data);
    }
    this.setState({
      data: data,
    });
  }

  // Other
  start(resource, channel) {
    //console.log(resource + " Data Starting");
    this.avatarCoreDataWrapper.startStreaming(
      this.onDataArrived.bind(this),
      resource,
      channel
    );
  }

  stop() {
    // console.log(this.props.resource + " Data Closing");
    this.avatarCoreDataWrapper.stopStreaming(function () {});
  }
}

export default withStyles(styles)(Data);
