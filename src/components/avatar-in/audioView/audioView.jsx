import React from "react";
import { Typography, Grid, withStyles, Button } from "@material-ui/core";

var coreAPI = require("../../../util/coreAPI");
import styles from "./audioView.styles.js";
import { relativeTimeRounding } from "moment";

class Audio extends React.Component {
  constructor(props) {
    super(props);
    this.start = this.start.bind(this);
    this.stop = this.stop.bind(this);
    this.avatarCoreAudioWrapper = coreAPI.CreateAudioWrapper();
    this.state = { length: 0, channels: 0, rate: 0, format: "", histogram: "" };
    this.audioPackets = new Uint8Array(4096);
  }

  componentDidMount() {
    this.start(this.props.resource);
  }

  componentWillUnmount() {
    this.stop();
    this.avatarCoreAudioWrapper.destroy();
    this.avatarCoreAudioWrapper = null;
  }

  render() {
    return (
      <React.Fragment>
        <div>
          <h1>Audio@ {this.props.resource}</h1>
          <h2>{JSON.stringify(this.state)}.</h2>
        </div>
      </React.Fragment>
    );
  }

  //*incomplete* function to convert from bytes to the corresponding audio sample value based on the sample size
  convertValue(i, samplesize) {
    if (samplesize == 1) {
      //single byte
      return this.audioPackets[i];
    }
    if (samplesize == 2) {
      //two bytes
      var sign = this.audioPackets[i] & (1 << 7);
      var x =
        ((this.audioPackets[i] & 0xff) << 8) |
        (this.audioPackets[i + 1] & 0xff);
      if (sign) {
        x = 0xffff0000 | x; // fill in most significant bits with 1's
      }
      return x;
    }
  }
  applyHistogram(binsize, samplesize) {
    let min = Infinity;
    let max = -Infinity;
    var length = this.audioPackets.length;

    for (var i = 0; i < length; i += samplesize) {
      var item = this.convertValue(i, samplesize);
      if (item < min) min = item;
      else if (item > max) max = item;
    }

    const bins = Math.ceil((max - min + 1) / binsize);

    const histogram = new Array(bins).fill(0);

    for (var i = 0; i < length; i += samplesize) {
      var item = this.convertValue(i, samplesize);
      histogram[Math.floor((item - min) / binsize)]++;
    }

    return histogram;
  }

  onAudioArrived(frame) {
    var length = frame.length;
    if (length > this.audioPackets.length) length = this.audioPackets.length;
    var remaining = this.audioPackets.length - length;
    for (var i = 0; i < remaining; ++i) {
      this.audioPackets[i] = this.audioPackets[i + length]; //offset the packets
    }
    for (var i = 0; i < length; ++i) {
      this.audioPackets[remaining + i] = frame[i];
    }

    var h = this.applyHistogram(128, coreAPI.AudioSampleSize[frame.format]);
    this.setState({
      length: frame.length,
      channels: frame.channels,
      rate: frame.rate,
      format: coreAPI.AudioFormatString[frame.format],
      histogram: JSON.stringify(h),
    });

    return;
    //not efficient method to concat the samples!
    //create new array to hold the samples
    var arr = new Uint8Array(this.audioPackets.length + frame.length);

    //copy the previous and new samples
    arr.set(this.audioPackets);
    arr.set(frame, this.audioPackets.length);
    var samplesCount = 1024 * AudioSampleSize[frame.format];
    //check if the samples length are more than a specific length, the length is determined by the sample size
    if (arr.length > samplesCount) {
      arr = arr.slice(0, arr.length - samplesCount);
    }
    this.audioPackets = arr;

    return;
    this.setState({
      length: frame.length,
      channels: frame.channels,
      rate: frame.rate,
      format: frame.format,
    });
  }

  // Other
  start(resource) {
    // console.log(resource + " Audio Starting");
    this.avatarCoreAudioWrapper.startStreaming(
      this.onAudioArrived.bind(this),
      resource
    );
  }

  stop() {
    //console.log(this.props.resource + " Audio Closing");
    this.avatarCoreAudioWrapper.stopStreaming(function () {});
  }
}

export default withStyles(styles)(Audio);
