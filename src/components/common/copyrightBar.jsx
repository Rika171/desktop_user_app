import React from "react";
import { Link, Typography, Grid, withStyles } from "@material-ui/core";



function CopyrightBar({ classes, ...props }) {
    return (
        <Grid item xs={12} className={classes.copyright}>
            <Typography variant="body2" color="textSecondary" align="center">
                {'Copyright © '}
                <Link href="http://www.avatarin.com">Avatar-in</Link>{' '}
                {new Date().getFullYear()}{'.'}
            </Typography>
        </Grid>
    );
}

const styles = theme => {
    copyright: {
        marginTop: theme.spacing(3)
    }
}

export default  withStyles(styles)(CopyrightBar);