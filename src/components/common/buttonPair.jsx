import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import variables from "./../../styles/variables.scss";
import { Button, Grid, withStyles } from '@material-ui/core';
import theme from '../../styles/theme';
import BorderButton from "./borderButton.jsx";
import SubmitButton from "./submitButton.jsx";
import { navigate } from "@reach/router";

const styles = {
  root: {},
  cancelBtn: {
    // borderColor: variables.gray282,
    marginRight: theme.spacing(2),
    padding: theme.spacing(2, 4),
  },
  submitBtn: {
    padding: theme.spacing(2, 4.3),
  }
};

function ButtonPair(props) {
  const { classes, submitTitle, cancelTitle, onPressSubmit, className, disabled, ...other } = props;
  const onClickCancel = e => {
    e.preventDefault();
    navigate(-1);
  }
  return (
    <Grid item xs={12} container justify="flex-end" className={className}>
        <BorderButton title={cancelTitle.toUpperCase()} className={classes.cancelBtn} onClick={onClickCancel}/>
        <SubmitButton
            disabled= {disabled}
            title={submitTitle.toUpperCase()}
            className={classes.submitBtn}
        />
    </Grid>
  );
}

ButtonPair.propTypes = {
  classes: PropTypes.object,
  className: PropTypes.string,
  submitTitle: PropTypes.string.isRequired,
  cancelTitle: PropTypes.string.isRequired,
};

export default withStyles(styles)(ButtonPair);