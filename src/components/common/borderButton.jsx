import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import variables from "./../../styles/variables.scss";
import { Button, withStyles } from '@material-ui/core';
import theme from '../../styles/theme';

const styles = {
  root: {
    // width: '95px',
    // height: "36px",
    borderRadius: "8px",
    border: "solid 1px",
    // border: 'solid 1px #ffffff',
    borderColor: variables.primaryAvatarin,
    fontFamily: "Archivo",
    fontSize: "0.875rem",
    fontWeight: "600",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.14",
    letterSpacing: "0.078em",
    textAlign: "center",
    color: variables.primaryAvatarin,
    padding: theme.spacing(1.2, 2.4),
  },
};

function BorderButton(props) {
  const { classes, title, className, onClick, ...other } = props;

  return (
    <Button className={clsx(classes.root, className)} onClick={onClick} {...other}>
      {title}
    </Button>
  );
}

BorderButton.propTypes = {
  children: PropTypes.node,
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  onClick: PropTypes.func,
};

export default withStyles(styles)(BorderButton);