import { Paper, Box } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import React from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import styles from "./styles";

class TopPanel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            anchorEl: null,
            language: 'en'
        }
    }

    handleLanguage = event => {
        let newLang = event.target.value;
        this.setState({ language: newLang })
        this.props.i18n.changeLanguage(newLang);
    }

    handleMenu = (event) => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    render() {
        let { classes, auth, t, i18n } = this.props;

        const languageProps = {
            onChange: this.handleLanguage,
            language: localStorage.getItem("lang") || i18n.language
        };
        const open = Boolean(this.state.anchorEl);

        return (
            <div className={classes.root} >
                <Box
                    bgcolor="grey.700"
                    position="absolute"
                    top={40}
                    left="80%"
                    zIndex="tooltip"
                >
                    <Paper variant="outlined" position="absolute" square />
                </Box>
            </div>
        );
    }
}

TopPanel.propTypes = {
    classes: PropTypes.object.isRequired
}

const TopPanelStyles = withStyles(styles)(TopPanel);
const mapStateToProps = state => ({
    auth: state.auth
});


export default connect(mapStateToProps)(withTranslation('translations')(TopPanelStyles));