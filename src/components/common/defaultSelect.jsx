import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import variables from "./../../styles/variables.scss";
import { Select, withStyles, FormControl, InputLabel, MenuItem } from '@material-ui/core';
import { withTranslation } from "react-i18next";

const styles = {
    root: {
        borderRadius: '6px',
        border: '1px',
        borderColor: variables.grayA9A,
        // backgroundColor: variables.gray9F9,
        width: "100%"
    },
    label: {
        fontFamily: 'Roboto',
        fontSize: '0.75rem',
        fontWeight: 'normal',
        fontStretch: 'normal',
        fontStyle: 'normal',
        lineHeight: 1.33,
        letterSpacing: '0.025em',
        color: variables.black06
    },
};

function DefaultSelectField(props) {
    const { t, id, classes, label, error, className, onChange, options, isObject, ...other } = props;
    console.log(t, 'AAAA')
    return (
        <FormControl variant="outlined" className={classes.root}>
            <InputLabel className={classes.label} id={id}>{label}</InputLabel>
            <Select className={classes.label}
                labelId={id}
                id={id}
                error={error && error.length > 0 ? true : false}
                className={className}
                fullWidth
                onChange={onChange}
                label={label}
                helperText={error}
            >
                {!isObject ? options.map((m, i) => (
                    <MenuItem className={classes.label} value={m}>{t(m)}</MenuItem>
                )) : options.map((m, i) => (
                    <MenuItem className={classes.label} value={m.code}>{m.name}</MenuItem>
                ))}
            </Select>
        </FormControl>
    );
}

DefaultSelectField.propTypes = {
    id: PropTypes.string.isRequired,
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
    label: PropTypes.string.isRequired,
    value: PropTypes.string,
    error: PropTypes.string,
    onChange: PropTypes.func.isRequired,
};

export default withStyles(styles)(withTranslation('translations')(DefaultSelectField));