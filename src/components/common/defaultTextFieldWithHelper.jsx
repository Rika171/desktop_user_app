import React, { useState }  from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import variables from "./../../styles/variables.scss";
import { TextField, withStyles } from '@material-ui/core';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import { Link } from "@reach/router";

const styles = {
  root: {
    borderRadius: '6px',
    border: '1px',
    borderColor: variables.grayA9A,
    backgroundColor: variables.gray9F9,
  },
  label: {
    fontFamily: 'Roboto',
    fontSize: '0.75rem',
    fontWeight: 'normal',
    fontStretch: 'normal',
    fontStyle: 'normal',
    lineHeight: 1.33,
    letterSpacing: '0.025em',
    color: variables.black06
  },
  helperText: {
    fontFamily: 'Roboto',
    fontSize: '0.75rem',
    fontWeight: 'normal',
    fontStretch: 'normal',
    fontStyle: 'normal',
    lineHeight: 1.33,
    letterSpacing: '0.025em',
    color: variables.black06,
    textDecoration: 'none',
  }
};

function DefaultTextFieldWithHelper(props) {
  const { id, classes, label, error, className, onChange, helperText, required, ...other } = props;

  const showError = error && error.length > 0;
  return (
    <FormControl fullWidth className={className} variant="outlined" error={showError ? true : false}>
      <InputLabel required={required} className={classes.label}>{label}</InputLabel>
      <OutlinedInput
        id={id}
        fullWidth
        required={required}
        className={classes.root}
        type='text'
        onChange={onChange}
        labelWidth={70}
        {...other}
      />
      {showError && <FormHelperText id="error-text">{error}</FormHelperText>}
      {!showError && helperText && helperText.length > 0 && <FormHelperText id="error-text" component={Link} to="/forgot-password" className={classes.helperText}>{helperText}</FormHelperText>}
    </FormControl>
  );
}

DefaultTextFieldWithHelper.propTypes = {
  id: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  label: PropTypes.string.isRequired,
  value: PropTypes.string,
  error: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  required: PropTypes.bool.isRequired,
  helperText: PropTypes.string,
};

export default withStyles(styles)(DefaultTextFieldWithHelper);