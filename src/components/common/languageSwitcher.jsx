import React from "react";
import PropTypes from "prop-types";
import { Select } from "@material-ui/core";


class LanguageSwitcher extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <React.Fragment>
                <Select native value={this.props.language} onChange={this.props.onChange } >
                    <option value="en">English</option>
                    <option value="jp">日本語</option>
                </Select>
            </React.Fragment>
        );
    }
}


export default LanguageSwitcher;