import { FormControl, FormControlLabel, FormLabel, Radio, RadioGroup, withStyles } from '@material-ui/core';
import PropTypes from 'prop-types';
import React from 'react';
import { withTranslation } from "react-i18next";
import variables from "./../../styles/variables.scss";

const styles = {
    root: {
        borderRadius: '6px',
        border: '1px',
        borderColor: variables.grayA9A,
        // backgroundColor: variables.gray9F9,
        width: "100%"
    },
    label: {
        fontFamily: 'Roboto',
        fontSize: '0.75rem',
        fontWeight: 'normal',
        fontStretch: 'normal',
        fontStyle: 'normal',
        lineHeight: 1.33,
        letterSpacing: '0.025em',
        color: variables.black06
    },
    radio: {
        '&$checked': {
            color: '#000000'
        }
    },
    checked: {}
};

function DefaultRadioGroupField(props) {
    const { t, id, classes, label, error, className, onChange, options, value, ...other } = props;
    return (
        <FormControl id={id} component="fieldset" variant="outlined" className={classes.root}>
            <RadioGroup className={classes.label} onChange={onChange}>
                {options.map((m, i) => (
                    <FormControlLabel key={i} className={classes.label} value={m} control={<Radio
                        classes={{ root: classes.radio, checked: classes.checked }} onChange={onChange} checked={value === m} />} label={t(m)} />
                ))}
            </RadioGroup>
        </FormControl>
    );
}

DefaultRadioGroupField.propTypes = {
    id: PropTypes.string,
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
    value: PropTypes.string,
    error: PropTypes.string,
    onChange: PropTypes.func.isRequired,
};

export default withStyles(styles)(withTranslation('translations')(DefaultRadioGroupField));