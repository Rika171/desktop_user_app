import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import variables from "./../../styles/variables.scss";
import { Button, withStyles } from '@material-ui/core';

const styles = {
  root: {
    padding: 0,
    fontFamily: "Archivo",
    fontSize: "1rem",
    fontWeight: "500",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.5",
    letterSpacing: "0.031em",
    color: variables.black039,
  },
};

function BorderLessButton(props) {
  const { classes, title, className, onClick, ...other } = props;

  return (
    <Button className={clsx(classes.root, className)} {...other} onClick={onClick}>
      {title}
    </Button>
  );
}

BorderLessButton.propTypes = {
  children: PropTypes.node,
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
};

export default withStyles(styles)(BorderLessButton);