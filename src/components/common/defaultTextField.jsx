import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import variables from "./../../styles/variables.scss";
import { TextField, withStyles } from '@material-ui/core';

const styles = {
  root: {
    borderRadius: '6px',
    border: '1px',
    borderColor: variables.grayA9A,
    backgroundColor: variables.gray9F9,
  },
  label: {
    fontFamily: 'Roboto',
    fontSize: '0.75rem',
    fontWeight: 'normal',
    fontStretch: 'normal',
    fontStyle: 'normal',
    lineHeight: 1.33,
    letterSpacing: '0.025em',
    color: variables.black06
  },
};

function DefaultTextField(props) {
  const { id, classes, label, error, className, onChange, ...other } = props;

  return (
    <TextField
      id={id}
      error={error && error.length > 0 ? true : false}
      label={label}
      fullWidth
      className={className}
      InputLabelProps={{
        className: classes.label
      }}
      InputProps={{
        className: classes.root
      }}
      onChange={onChange}
      variant="outlined"
      helperText={error}
      {...other}
    />
  );
}

DefaultTextField.propTypes = {
  id: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  label: PropTypes.string.isRequired,
  value: PropTypes.string,
  error: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

export default withStyles(styles)(DefaultTextField);