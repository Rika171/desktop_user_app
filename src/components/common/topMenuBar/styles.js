import variables from "./../../../styles/variables.scss";

export default theme => ({
  root: {
    flexGrow: 1,
  },
  title: {
    fontSize: '1.5rem',
    fontWeight: '500',
    fontStretch: 'normal',
    fontStyle: 'normal',
    lineHeight: 1.2,
    letterSpacing:'0.009375em',
    color: variables.primaryAvatarin,
    textDecoration: 'none',
    flexGrow: 1,
  },
  appBar: {
    // backgroundColor: variables.gray6A8,
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(2),
    backgroundColor: variables.white25595,
  },
  toolBar: {
    padding: theme.spacing(0),
  },
  avatarinYourBusinessBtn: {
    marginRight: '1.9%',
  },
  signUpBtn: {
    marginRight: '1.7%',
  },
  moreVertBtn: {
    padding: 0,
    color: variables.primaryAvatarin,
    marginRight: '0.7%',
  },
  loginBtn: {
    padding: theme.spacing(1.2, 3),
  },
  panel: {
    height: '15.00rem',
    width: '25.4375rem'
  },

  panelOut: {
    width: '25.4375rem',
  },

  topMenu: {
    top: "32px !important"
  },

  userAccount:{
    color: "#0066cc"
  },

  avatar: {
    height: '3.5rem',
    width: '3.5rem'
  },
  container: {
    display: 'grid',
    gridTemplateColumns: 'repeat(12, 1fr)',
    gridGap: theme.spacing(3),
  },
  avatarContainner: {
    padding: 12
  },
  row: {
    padding: " 1rem"
  },
  fade_text: {
    fontFamily: "Roboto",
    fontSize: "0.875rem",
    fontWeight: "normal",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.43",
    letterSpacing: "normal",
    color: "#a5a6a8"
  },
  arrow_r: {
    height: "0.75rem",
    color: variables.black000,
  },
  p_2: {
    paddingLeft: "1.4rem !important",
  },
  link: {
    color: "#1a6fd0",
    textDecoration: 'none',
  },
  align_right: {
    float: 'right'
  },
  menu_panel: {
    borderRadius:"16px",
    boxShadow:"3px -5px 40px 0 rgba(59, 59, 63, 0.09)",
    border:"solid 1px",
    borderColor: variables.gray2ff,
    backgroundColor: variables.white,
  },
  menu_item_name: {
    fontWeight: "500",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.71",
    color: variables.black039,
  },
  menu_item_value: {
    fontWeight: "500",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.71",
    color: variables.black666,
  },
  breakLine: {
    borderRadius: "16px",
    boxShadow: "3px -5px 40px 0 rgba(59, 59, 63, 0.09)",
    border: "solid 0.1px",
    borderColor: variables.gray2ff,
    backgroundColor: variables.white,
    // height: '0.1px',
  },
  panel_bottom_text: {
    color: variables.black039,
    textDecoration: 'none',
  },
  panel_bottom_text_break: {
    color: variables.black039,
    // marginHorizontal : "0.5em",
  },
  arrow_icon: {
    padding: 0,
    color: variables.primaryAvatarin,
    marginRight: theme.spacing(2.1),
  },
  logo_img: {
    width: '96px',
    height: '18px',
  },
  title_con: {
    flexGrow: 1,
  } 
});