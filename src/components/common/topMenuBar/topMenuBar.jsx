import { AppBar, Divider, Grid, Menu, MenuItem, Toolbar, Typography } from "@material-ui/core";
import Avatar from '@material-ui/core/Avatar';
import IconButton from "@material-ui/core/IconButton";
import { withStyles } from "@material-ui/core/styles";
import AccountCircle from "@material-ui/icons/AccountCircle";
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import InputIcon from '@material-ui/icons/Input';
import LanguageIcon from '@material-ui/icons/Language';
import { spacing } from '@material-ui/system';
import MoreVert from "@material-ui/icons/MoreVert";
import NaturePeopleIcon from '@material-ui/icons/NaturePeople';
import PersonIcon from '@material-ui/icons/Person';
import { Link } from "@reach/router";
import PropTypes from "prop-types";
import React from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { signOut } from "../../../_actions/user.action";
import BorderButton from '../borderButton.jsx';
import BorderLessButton from '../borderLessButton.jsx';
import styles from "./styles";
import './topMenuBar.sass';
import { navigate } from "../../../../src/util";
import ArrowBack from "@material-ui/icons/ArrowBack";
import avatatinLogoImg from "../../../assets/images/avatatin_logo.png";
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
class TopMenuBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            anchorEl: null,
        }
    }

    componentDidMount() {
        const user = localStorage.getItem('user');
        if (user) {
            this.setState({
                user: JSON.parse(user)
            });
        }
    }

    handleLanguage = event => {
        let newLang = event.target.value;
        this.setState({ language: newLang })
        this.props.i18n.changeLanguage(newLang);
    }

    handleMenu = (event) => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    openLangSelector = () => {
        // this.setState({ lang: true });
    }

    handleNavBack = (e) => {
        navigate(-1);
    };

    render() {
        let { classes, auth, t, i18n } = this.props;
        const currentSelectedLan = localStorage.getItem("lang") || i18n.language;
        let { user } = this.state;
        const cur = localStorage.getItem("cur") || 'yen'
        const open = Boolean(this.state.anchorEl);
        let fullname = "Visitor";
        if(user){
            const {firstname, lastname} = user;
            if(firstname){
                fullname = firstname;
            }
            if(lastname)
                fullname = fullname + " "+ lastname;
        }

        return (
            <AppBar className={classes.appBar} elevation={0}>
                <Toolbar className={classes.toolBar}>
                    <Typography className={classes.title_con} component={Link} to="./">
                        <img className={classes.logo_img} src={avatatinLogoImg} />
                    </Typography>
                    {/* <Typography className={classes.title} component={Link} to="./">
                        {t("title")}
                    </Typography> */}
                    {auth.isAuthenticated ? (
                        <div>
                            <IconButton
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={this.handleMenu}
                                color="inherit"
                                edge="end"
                                className={classes.userAccount}
                            >
                                <AccountCircle />
                            </IconButton>

                            <Menu
                                className={classes.topMenu}
                                id="menu-appbar"
                                anchorEl={this.state.anchorEl}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={open}
                                onClose={this.handleClose}
                                PaperProps={{
                                    className: classes.menu_panel
                                }}
                            >
                                <div className={classes.panel}>
                                    <Grid container spacing={3} className={classes.avatarContainner}>
                                        <Grid item xs={2} className={classes.p_2}>
                                            <Avatar className={classes.avatar}>
                                                <PersonIcon />
                                            </Avatar>
                                        </Grid>
                                        <Grid item xs={9} className={classes.p_2}>
                                            <Typography component="h5" variant="h5">{fullname }</Typography>
                                            <Typography component="div" variant="body2">{(user || {}).username}</Typography>
                                        </Grid>
                                    </Grid>
                                    <Divider className={classes.breakLine} />
                                    <Grid container alignItems="center" className={classes.row}>
                                        <Grid item xs={2}>
                                            <InputIcon />
                                        </Grid>
                                        <Grid item xs={10}>
                                            <MenuItem onClick={this.handleClose} component={Link} to="/signout">{t("logout")}</MenuItem>
                                        </Grid>
                                    </Grid>
                                    <Divider className={classes.breakLine} />
                                    <Grid container alignItems="center" className={classes.row}>
                                        <Grid item xs={12}>
                                            <MenuItem onClick={this.openLangSelector} component={Link} to="/change-lan" onClick={this.handleClose}>
                                                <Grid item xs={2} >
                                                    <LanguageIcon />
                                                </Grid>
                                                <Grid item xs={5}>
                                                    <Typography component="div" variant="subtitle2" className={classes.menu_item_name}>{t("mLanguage")}</Typography>
                                                </Grid>
                                                <Grid item xs={5}>
                                                    <Typography component="div" variant="subtitle2" className={classes.menu_item_value}>{t(currentSelectedLan)} </Typography>
                                                </Grid>
                                                <Grid item xs={2}>
                                                    <Typography component="div" variant="subtitle2" className={classes.align_right}><ArrowForwardIosIcon className={classes.arrow_r} /></Typography>
                                                </Grid>
                                            </MenuItem>
                                        </Grid>
                                    </Grid>
                                </div>
                            </Menu>
                        </div>
                    ) : (
                            <>
                                <IconButton
                                    aria-haspopup="true"
                                    onClick={this.handleMenu}
                                    className={classes.moreVertBtn}
                                >
                                    <MoreVert />
                                </IconButton>
                                <Menu
                                    className={classes.topMenu}
                                    id="menu-appbar"
                                    anchorEl={this.state.anchorEl}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    keepMounted
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open={open}
                                    onClose={this.handleClose}
                                    PaperProps={{
                                        className: classes.menu_panel
                                    }}
                                >
                                    <div className={classes.panelOut}>
                                        <Grid container alignItems="center" className={classes.row}>
                                            <Grid item xs={12}>
                                                <MenuItem onClick={this.openLangSelector} component={Link} to="/change-lan" onClick={this.handleClose}>
                                                    <Grid item xs={2} >
                                                        <LanguageIcon />
                                                    </Grid>
                                                    <Grid item xs={5}>
                                                        <Typography component="div" variant="subtitle2" className={classes.menu_item_name}>{t("mLanguage")}</Typography>
                                                    </Grid>
                                                    <Grid item xs={5}>
                                                        <Typography component="div" variant="subtitle2" className={classes.menu_item_value}>{t(currentSelectedLan)} </Typography>
                                                    </Grid>
                                                    <Grid item xs={2}>
                                                        <Typography component="div" variant="subtitle2" className={classes.align_right}><ArrowForwardIosIcon className={classes.arrow_r} /></Typography>
                                                    </Grid>
                                                </MenuItem>
                                            </Grid>
                                        </Grid>
                                    </div>
                                </Menu>
                            </>
                        )}
                </Toolbar>
            </AppBar>
        );
    }
}

TopMenuBar.propTypes = {
    classes: PropTypes.object.isRequired
}

const TopMenuBarStyles = withStyles(styles)(TopMenuBar);
const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps, { signOut })(withTranslation('translations')(TopMenuBarStyles));