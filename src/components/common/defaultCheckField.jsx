import React, { useState }  from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import variables from "./../../styles/variables.scss";
import { TextField, withStyles, Checkbox, FormControlLabel, Box, Typography} from '@material-ui/core';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import { Link } from "@reach/router";
import theme from '../../styles/theme';
import captchaImg from "../../assets/images/captcha.png";

const styles = {
  root: {
    borderRadius: "6px",
    border: "solid 1px rgba(0, 0, 0, 0.38)",
    height: '56px',
    justifyContent: 'center',
  },
  label: {
    fontSize: "1rem",
    fontWeight: "500",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.5",
    letterSpacing: "0.5px",
    color: variables.black039,
    width: '100%'
  },
  helperText: {
    fontFamily: 'Roboto',
    fontSize: '0.75rem',
    fontWeight: 'normal',
    fontStretch: 'normal',
    fontStyle: 'normal',
    lineHeight: 1.33,
    letterSpacing: '0.025em',
    color: variables.black06,
    textDecoration: 'none',
  },
  captchaImg: {
    width: '24px',
    height: '24px',
    marginRight: theme.spacing(2),
  },
  boxCon: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  checkBox: {
    borderColor: "rgba(0, 0, 0, 0.6)",
  }
};

function defaultCheckField(props) {
  const { id, classes, label, error, className, onChange, helperText, required, value, ...other } = props;

  const showError = error && error.length > 0;
  return (
    <FormControl fullWidth className={`${classes.root} ${className}`} variant="outlined" error={showError ? true : false}>
      <Box className={classes.boxCon}>
        <Checkbox id={id} className={classes.checkBox} onChange={onChange} value color="primary"/>
        <Typography className={classes.label}>
            {label}
        </Typography>
        <img className={classes.captchaImg} src={captchaImg}/>
      </Box>
    </FormControl>
  );
}

defaultCheckField.propTypes = {
  id: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  label: PropTypes.string.isRequired,
  value: PropTypes.string,
  error: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  required: PropTypes.bool.isRequired,
  helperText: PropTypes.string,
};

export default withStyles(styles)(defaultCheckField);