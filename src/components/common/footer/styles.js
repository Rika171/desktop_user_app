import variables from "../../../styles/variables.scss";

export default theme => ({
  root: {
    flexGrow: 1,
    display: 'flex',
    position: 'fixed',
    bottom: 0,
    width: '100%',
    borderTop: "solid 1px",
    borderColor: variables.gray7E7,
    backgroundColor: variables.white,
  },
  con: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  text: {
    textDecoration: 'none',
    marginLeft: theme.spacing(3),
  }
});