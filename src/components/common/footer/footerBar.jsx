import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles";
import { connect } from "react-redux";
import { Grid, Typography } from "@material-ui/core";
import { Link } from "@reach/router";
import { withTranslation } from "react-i18next";

class Footer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let { classes, auth , t, i18n } = this.props;
        const currentYear = new Date().getFullYear();
        return (
            <div className={classes.root}>
                <Grid container justify="center" alignItems="center" className={classes.con}>
                    <Typography variant="caption" className={classes.text} component={Link} to="/about-avatarin">
                        {t("aboutAvatarin")}
                    </Typography>
                    <Typography variant="caption" className={classes.text} component={Link} to="/conditions-use">
                        {t("conditionsOfUse")}
                    </Typography>
                    <Typography variant="caption" className={classes.text} component={Link} to="/privacy-notice">
                        {t("privacyNotice")}
                    </Typography>
                    <Typography variant="caption" className={classes.text} component={Link} to="/help">
                        {t("help")}
                    </Typography>
                    <Typography variant="caption" className={classes.text} component={Link} to="/contact">
                        {`${t("contact")} Avatarin`}
                    </Typography>
                    <Typography variant="caption" className={classes.text}>
                        {`© ${currentYear - 1}-${currentYear}, Avatarin.com, Inc. or its affiliates`}
                    </Typography>
                </Grid>
            </div>
        );
    }
}

Footer.propTypes = {
    classes: PropTypes.object.isRequired
}

const FooterStyles = withStyles(styles)(Footer);

export default withTranslation('translations') (FooterStyles);