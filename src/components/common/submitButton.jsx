import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import variables from "./../../styles/variables.scss";
import { Button, withStyles } from '@material-ui/core';
import theme from '../../styles/theme';

const styles = {
  root: {
    fontFamily: 'Archivo',
    border: 'none',
    padding: theme.spacing(2, 14),
    borderRadius: "8px",
    // backgroundImage: "linear-gradient(286deg, #0056ab 92%, #0066cc 22%)",
    fontSize: "0.875rem",
    fontWeight: "600",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.14",
    letterSpacing: "0.078em",
    textAlign: "center",
    color: "#fff !important",
    backgroundColor: variables.primaryAvatarin,
  },
};

function SubmitButton(props) {
  const { classes, title, className, ...other } = props;

  return (
    <Button
      type="submit"
      // fullWidth
      variant="contained"
      className={clsx(classes.root, className)} {...other}>
      {title}
    </Button>
  );
}

SubmitButton.propTypes = {
  children: PropTypes.node,
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
};

export default withStyles(styles)(SubmitButton);