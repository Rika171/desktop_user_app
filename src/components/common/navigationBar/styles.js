import variables from "./../../../styles/variables.scss";

export default theme => ({
  root: {
    display: 'flex',
    backgroundColor: variables.white,
    height: '64px',
    border: 'solid 1px',
    borderColor: variables.gray7E7,
    width: '100%',
    marginTop: theme.spacing(8),
    paddingLeft: theme.spacing(2),
  },
  arrowIcon: {
    padding: 0,
    color: variables.black087,
    marginRight: theme.spacing(2.5),
  },
  title: {
    fontWeight: 'bold',
  }
});