import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles";
import './navigationBar.sass';
import { connect } from "react-redux";
import { Grid, Typography } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import ArrowBack from "@material-ui/icons/ArrowBack";
import { withTranslation } from "react-i18next";
import BorderButton from '../borderButton.jsx';
import BorderLessButton from '../borderLessButton.jsx';
import theme from "../../../styles/theme";
import { Link, navigate } from "@reach/router";

class NavigationBar extends React.Component {

    handleNavBack = (e) => {
        navigate(-1);
    };

    render() {
        let { classes, auth , t, i18n, title } = this.props;

        return (
            <div className={classes.root}>
                <Grid container justify="flex-start" alignItems="center">
                    <IconButton
                        className={classes.arrowIcon}
                        onClick={this.handleNavBack}
                    >
                        <ArrowBack />
                    </IconButton>
                    <Typography variant="h6" className={classes.title} >
                        {title}
                    </Typography>
                </Grid>
            </div>
        );
    }
}

NavigationBar.propTypes = {
    classes: PropTypes.object.isRequired
}

const NavigationBarStyles = withStyles(styles)(NavigationBar);

export default withTranslation('translations') (NavigationBarStyles);