import { createHistory ,createMemorySource } from "@reach/router";

let source = createMemorySource("/");
export const history = createHistory(source);
export const navigate = history.navigate;