import avatarcoreAPI from "../../build/Release/AvatarCoreAPI.node";
//var avatarcoreAPI = require("bindings")("AvatarCoreAPI");
//import avatarcoreAPI from CoreAddon

const { ipcRenderer } = require("electron");

var coreAPIWrapper;

var core_inited = false;
var core_initing = false;

var _service_status = {};

function ServiceStatusReplyCB(service, path, data) {
  var res = data.split(":");
  _service_status[res[0]] = res[1] == "1" ? true : false;
  /*
  process.stdout.write("ServiceStatusReplyCB:  " + res[0] + " = ");
  process.stdout.write(_service_status[res[0]].toString());
  process.stdout.write("\n");*/
}

//predefined callback types
const DefaultCallbacks = {
  InitCallbackType: "Init",
  DestroyCallbackType: "Destroy",
};

var callbacks = {};

//Add a callback function with the format
// function(){}
function AddCallbackFunction(callbackType, func) {
  if (!(callbackType in callbacks)) {
    callbacks[callbackType] = [];
  }
  var index = callbacks[callbackType].indexOf(func);
  if (index == -1) {
    callbacks[callbackType].push(func);
  }
}

//Remove an existing callback function
function RemoveCallback(callbackType, func) {
  if (!(callbackType in callbacks)) {
    return;
  }
  var index = callbacks[callbackType].indexOf(func);
  if (index != -1) {
    callbacks[callbackType].splice(index, 1);
  }
}

function TriggerCallback(callbackType) {
  process.stdout.write("------> TriggerCallback [" + callbackType + "]\n");
  if (callbackType in callbacks) {
    callbacks[callbackType].forEach(function (value) {
      value();
    });
  }
}

//predefined list of services
const ServiceNames = {
  CloudProxy: "RTP/proxy",
  VideoCapture: "Capture/video/front",
  AudioCapture: "Capture/audio",
  MediaSender: "RTP/sender/media",
  MediaReceiver: "RTP/receiver/media",
  DataSender: "RTP/sender/data",
  DataReceiver: "RTP/receiver/data",
  AudioPlayback: "Capture/remote/audio",
};

/*
This function invokes a message to check whether a certain service is running or not, and will return its response in an asynced manner
Sample usage of this function:
checkServiceStatus(service, function (res) {
  console.log(service + " : " + res);
});
*/
function CheckServiceStatus(service, callback, timeout = 10) {
  _service_status[service] = false;
  coreAPIWrapper.PublishMessage("", "service/status/get", service);
  setTimeout(function () {
    callback(_service_status[service]);
  }, timeout);
}

//Initialize core API, master should be true as it indicates to start the service coordinator as a master service
function InitAPI(master, path = "") {
  if (core_inited || core_initing) return;
  process.stdout.write("Javascript- InitAPI!\n");
  //Create Core API Wrapper instance here, its a singleton
  coreAPIWrapper = new avatarcoreAPI.NapiCoreAPIWrapper();
  core_initing = true;
  //Initialize Core
  coreAPIWrapper.InitAPI(master, path);

  //Load Services, two approaches are possible:
  // LoadServices --> Would load the services to the same process, still have some issues
  // RefService --> Load the services via a subprocess, more stable but have the risk of the subprocess be active even when the main process is closed.

  // coreAPIWrapper.LoadServices("services/Management-services.json");
  coreAPIWrapper.LoadServices("services/Capture-services.json");
  //coreAPIWrapper.LoadServices("services/RTP-services.json");
  //coreAPIWrapper.RefService("Management");
  //coreAPIWrapper.RefService("Capture");
  coreAPIWrapper.RefService("RTP");
  //coreAPIWrapper.RefService("Robot"); //only use with Robot App

  //subscribe to the status reply messages, used to keep track of the status of the services when queried via "checkServiceStatus" function
  coreAPIWrapper.SubscribeToMessage(
    "Application",
    "service/status/reply",
    ServiceStatusReplyCB
  );

  core_initing = false;
  core_inited = true;
  //Trigger init callback function for subscribed functions
  TriggerCallback(DefaultCallbacks.InitCallbackType);
}

//Destroy core API and close the active services
function DestroyAPI() {
  if (core_inited == false) return;
  //Trigger on destroyed callback event
  TriggerCallback(DefaultCallbacks.DestroyCallbackType);

  //remove subscribed messages
  coreAPIWrapper.UnsubscribeToMessage("Application", "service/status/reply");

  process.stdout.write("Javascript- DestroyAPI!\n");
  core_inited = false;
  //Stop all capture and RTP services
  coreAPIWrapper.StopService("Capture/.*");
  coreAPIWrapper.StopService("RTP/.*");

  //Destroy the running subprocess sservices
  coreAPIWrapper.PublishMessage("", "serviceloader/kill", "");

  process.stdout.write("Javascript- coreAPIWrapper.UnrefService Capture!\n");
  coreAPIWrapper.UnrefService("Capture");
  process.stdout.write("Javascript- coreAPIWrapper.UnrefService RTP!\n");
  coreAPIWrapper.UnrefService("RTP");
  process.stdout.write("Javascript- coreAPIWrapper.UnrefService Robot!\n");
  coreAPIWrapper.UnrefService("Robot");
  process.stdout.write("Javascript- Core DestroyAPI\n"); /**/

  //Destroy the Core API instance
  coreAPIWrapper.DestroyAPI();

  avatarcoreAPI.KillProcess();
  process.stdout.write("Javascript- DestroyAPI - Done!\n");
  coreAPIWrapper = null;
}

//Shutdown the cloud process
function DestroyCloud() {
  process.stdout.write("Javascript- Destroy Cloud\n");
  avatarcoreAPI.CloseCloud();
  process.stdout.write("Javascript- Cloud Closed - Done!\n");
}

///Formats for audio
const AudioFormatString = {
  0: "Unkown",
  1: "U8", //single byte, unsigned
  2: "S8", //single byte, signed
  3: "U16", //two bytes
  4: "S16", //two bytes
  5: "U24", //three bytes
  6: "S24", //three bytes
  7: "U32", //four bytes
  8: "S32", //four bytes
  9: "F32", //four bytes, floating point
};
const AudioSampleSize = {
  0: 1,
  1: 1, //single byte, unsigned
  2: 1, //single byte, signed
  3: 2, //two bytes
  4: 2, //two bytes
  5: 3, //three bytes
  6: 3, //three bytes
  7: 4, //four bytes
  8: 4, //four bytes
  9: 4, //four bytes, floating point
};

function CreateVideoWrapper() {
  return new avatarcoreAPI.NapiVideoWrap();
}

function CreateAudioWrapper() {
  return new avatarcoreAPI.NapiAudioWrap();
}
function CreateDataWrapper() {
  return new avatarcoreAPI.NapiDataWrap();
}

function LoadServices(path) {
  if (core_inited == false) {
    process.stdout.write("LoadServices: CoreAPI is not inited!\n");
    return;
  }
  coreAPIWrapper.LoadServices(path);
}
function RefService(service) {
  if (core_inited == false) {
    process.stdout.write("RefService: CoreAPI is not inited!\n");
    return;
  }
  coreAPIWrapper.RefService(service);
}
function UnrefService(service) {
  if (core_inited == false) {
    process.stdout.write("UnrefService: CoreAPI is not inited!\n");
    return;
  }
  coreAPIWrapper.UnrefService(service);
}

function SubscribeToMessage(name, path, callback) {
  if (core_inited == false) {
    process.stdout.write("SubscribeToMessage: CoreAPI is not inited!\n");
    return;
  }
  coreAPIWrapper.SubscribeToMessage(name, path, callback);
  process.stdout.write("SubscribeToMessage: Message subscribed!\n");
}

function UnsubscribeToMessage(name, path) {
  if (core_inited == false) {
    process.stdout.write("UnsubscribeToMessage: CoreAPI is not inited!\n");
    return;
  }
  coreAPIWrapper.UnsubscribeToMessage(name, path);
}

function StartService(service) {
  if (core_inited == false) {
    process.stdout.write("StartService: CoreAPI is not inited!\n");
    return;
  }
  coreAPIWrapper.RunService(service);
}
function StopService(service) {
  if (core_inited == false) {
    process.stdout.write("StopService: CoreAPI is not inited!\n");
    return;
  }
  coreAPIWrapper.StopService(service);
}
function PublishMessage(target, path, data) {
  if (core_inited == false) {
    process.stdout.write("PublishMessage: CoreAPI is not inited!\n");
    return;
  }
  coreAPIWrapper.PublishMessage(target, path, data);
}

function SetAppConfigurationFilePath(path, isRelative) {
  if (core_inited == false) {
    process.stdout.write("SetAppConfigurationFilePath: CoreAPI is not inited!\n");
    return;
  }
  coreAPIWrapper.SetAppConfigurationFilePath( path, isRelative);
}

function SetAppConfiguation(name, value) {
  if (core_inited == false) {
    process.stdout.write("SetAppConfiguation: CoreAPI is not inited!\n");
    return;
  }
  coreAPIWrapper.SetAppConfiguation(name, value);
}


function GetAppConfiguation(name, defaultValue) {
  if (core_inited == false) {
    process.stdout.write("GetAppConfiguation: CoreAPI is not inited!\n");
    return;
  }
  coreAPIWrapper.GetAppConfiguation(name, defaultValue);
}

function IsCoreInited() {
  return core_inited;
}

export {
  InitAPI,
  DestroyAPI,
  IsCoreInited,
  DestroyCloud,
  CheckServiceStatus,
  CreateVideoWrapper,
  CreateAudioWrapper,
  CreateDataWrapper,
  LoadServices,
  RefService,
  UnrefService,
  SubscribeToMessage,
  UnsubscribeToMessage,
  StartService,
  StopService,
  PublishMessage,
  ServiceNames,
  AddCallbackFunction,
  DefaultCallbacks,
  AudioFormatString,
  AudioSampleSize,
  
  SetAppConfigurationFilePath,
  SetAppConfiguation,
  GetAppConfiguation
};
