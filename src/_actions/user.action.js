import qs from "qs";
import {USER_CONSTS} from "../_constants";
import {setAuthToken, saveAllTokens ,saveUserDetails, getAccessToken} from "../helpers/TokenHelper";
import AuthService from "../_services/auth.service";
import {navigate} from "../util";

export const signIn = userData => dispatch => {
    dispatch(request(userData));

    AuthService.loginUser( userData).then(res =>{
        const {access_token, refresh_token}  = res.data;
        setAuthToken(access_token);
        saveAllTokens({access_token,refresh_token});

        AuthService.getUserDetails().then(res =>{
            saveUserDetails(res.data);
            dispatch(success(res.data));
            navigate("/");
        })
        .catch(err=>{
            let payload = err;
            dispatch(error(payload));
        });
    })
    .catch(err=>{
        console.log("error occurred");
        console.log(err);
        dispatch(error(err));
    })

    function request(user){
        return {type: USER_CONSTS.LOGIN_REQUEST , user};
    }

    function success(user){
        return {type: USER_CONSTS.LOGIN_SUCCESS, user};
    }

    function error(user){
        return {type: USER_CONSTS.LOGIN_ERROR, user};
    }
}

export const signOut = () => dispatch =>{
    /*dispatch(request());
    let token = getAccessToken();
    AuthService.logoutUser(qs.stringify({"accessToken":token})).then(
        res =>{
            dispatch(success(res));
        }
    )
    .catch(
        err =>{
            dispatch(error(err));
        }
    )*/

    dispatch(success());
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
    localStorage.removeItem('user');
    navigate("/");


    function request(){
        console.log("--logout request");
        return {type:USER_CONSTS.LOGOUT_REQUEST};
    }

    function success(status){
        console.log("--logout success");
        return {type:USER_CONSTS.LOGOUT_SUCCESS, status};
    }

    function error(err){
        console.log("--logout error");
        return {type:USER_CONSTS.LOGOUT_ERROR, err};
    }

}

export const register = userData => dispatch =>{
    dispatch(request(userData));

    AuthService.registerUser(userData).then(res =>{
        navigate("/emil-confirm", { state: { email: userData.username } });
    })
    .catch(err=>{
        dispatch(error(err))
    });
    
    function request(data) { return {type:USER_CONSTS.REGISTER_REQUEST, data};}
    function success(data) { return {type:USER_CONSTS.REGISTER_SUCCESS, data};}
    function error(error) { return { type:USER_CONSTS.REGISTER_ERROR, error};}
}