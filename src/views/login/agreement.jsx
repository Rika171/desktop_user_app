import React from "react";
import PropTypes from "prop-types";
import {withTranslation} from "react-i18next";

class AgreementModal extends React.Component{

    constructor(props){
        super(props);
        this.state ={
            isAgree: false
        }
    }

    render(){
        const { t , i18n} = this.props;
        return (
            <div>
                <modal>

                </modal>
            </div>
        )
    }
}

AgreementModal.PropTypes = {
    isOpen: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired
}

export default withTranslation('translations')(AgreementModal);