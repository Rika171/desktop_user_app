import variables from "../../styles/variables.scss";

export default theme => ({
    paper: {
        width: 586,
        // height: 604,
        // marginTop: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        border: 'solid 1px',
        padding: theme.spacing(3.2, 12),
        borderRadius: "16px",
        boxShadow: "3px -5px 40px 0 rgba(59, 59, 63, 0.09)",
        borderColor: variables.gray2ff,
        backgroundColor: variables.white,
    },
    title: {
        // fontFamily: 'Futura',
        fontSize: '1.125rem',
        fontWeight: '500',
        fontStretch: 'normal',
        fontStyle: 'normal',
        lineHeight: 'normal',
        letterSpacing: 'normal',
        color: variables.grayE6f,
        marginBottom: theme.spacing(4),
    },
    form: {
        width: "100%",
        marginTop: theme.spacing(8),
        justify: "center",
        alignItems: "center",
        display: 'flex',
        flexDirection: 'column'
    },
    submit: {
        marginTop: theme.spacing(8),
    },
    textInput : {
        marginBottom: theme.spacing(5)
    },
    passwordInput : {
        marginBottom: theme.spacing(2.5)
    },
    recaptcha: {

    },
    signUpTextCon: {
        // backgroundColor: 'red',
        // height: 0,
    },
    signUpMainText: {
        marginTop: theme.spacing(2),
        color: variables.blank842
    },
    signUpText: {
        color: variables.blank842
    },
    body_main: {
        position: "absolute",
        top: "0",
        bottom: "0",
        left: "0",
        right: "0",
        backgroundImage: "linear-gradient(64deg, rgba(194, 241, 255, 0.44) 5%, rgba(236, 246, 252, 0.06) 48%, rgba(255, 85, 119, 0.17) 97%)"
    },
    login_bg_img: {
        width: '100%',
        height: '50%',
        borderBottomLeftRadius: "20px",
        borderBottomRightRadius: "20px",
    },
    body_con: {
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)"
    },
    login_title: {
        fontSize: "3rem",
        fontWeight: "normal",
        fontStretch: "normal",
        fontStyle: "normal",
        lineHeight: "1.08",
        letterSpacing: "normal",
        textAlign: "center",
        color: variables.primaryVariantAvatarin,
        // fontFamily: "ArchivoBlack",
    },
    logo_img: {
        width: '96px',
        height: '18px',
        marginBottom: theme.spacing(4),
    },
});