import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline"
import { withTranslation } from 'react-i18next';
import { Link } from "@reach/router";
import { Button, Grid, Typography, Container, withStyles, TextField } from "@material-ui/core";
import LanguageSwitcher from "../../components/common/languageSwitcher.jsx";
import Copyright from "../../components/common/copyrightBar.jsx";
import DefaultTextField from "../../components/common/defaultTextField.jsx";
import DefaultPasswordField from "../../components/common/defaultPasswordField.jsx";
import styles from "./login.styles";
import SubmitButton from "../../components/common/submitButton.jsx";
import NavigationBar from "../../components/common/navigationBar";
import loginBgImg from "../../assets/images/login_bg.jpg";
import DefaultCheckField from "../../components/common/defaultCheckField.jsx";
import avatatinLogoImg from "../../assets/images/avatatin_logo.png";

class LoginView extends React.Component {

    onChangeCaptcha = value => {
        console.log("Captcha value:", value);
    }
    render() {
        const { classes, i18n, t ,formValid } = this.props;

        return (
            <>
                {/* <NavigationBar title={t('logIn')} /> */}
                <Grid container component="main" className={classes.body_main}>
                    <img  className={classes.login_bg_img} src={loginBgImg}/>
                    <CssBaseline />
                    <Grid item container xs={12} justify="center" alignItems="center" direction="column" className={classes.body_con}>
                        <div className={classes.paper}>
                            {/* <Typography className={classes.title}>
                                {t('title')}
                            </Typography> */}
                            <Typography component={Link} to="./">
                                <img className={classes.logo_img} src={avatatinLogoImg}/>
                            </Typography>
                            <Typography component="h1" variant="h3" className={classes.login_title}>
                                {t('login')}
                            </Typography>

                            <form className={classes.form} noValidate onSubmit={this.props.handleSignIn}>
                                <DefaultTextField
                                    required
                                    id="email"
                                    label={t('emailaddress')}
                                    onChange = {this.props.onChange}
                                    name="email"
                                    autoComplete="email"
                                    autoFocus
                                    error={t(this.props.errors.email)}
                                    className={classes.textInput}
                                />
                                <DefaultPasswordField
                                    required={true}
                                    id="password"
                                    label={t('password')}
                                    onChange = {this.props.onChange}
                                    name="password"
                                    error={t(this.props.errors.password)}
                                    className={classes.textInput}
                                    // helperText={t('forgotpassword')}
                                />
                                <DefaultCheckField
                                    required
                                    id="userCheck"
                                    label={t('INotAvatar')}
                                    onChange = {this.props.onChange}
                                    name="userCheck"
                                    error={t(this.props.errors.userCheck)}
                                    value={this.props.userCheck}
                                />
                                <SubmitButton
                                    className={classes.submit}
                                    disabled= {!formValid}
                                    title={t('login')}
                                />
                            </form>
                        </div>
                    </Grid>
                </Grid>
            </>
        );
    }
}

export default withStyles(styles)(withTranslation('translations') (LoginView));