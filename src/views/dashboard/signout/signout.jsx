import React from "react";
import { Button, CssBaseline, Container, withStyles, Grid, Typography } from "@material-ui/core";
import styles from "./signout.styles";


class SignOut extends React.Component {
    render() {
        const { t, classes, handleCancel,handleSignout } = this.props;
        return (
            <>
                <Grid container component="main" className={classes.body_main}>
                    <CssBaseline />
                    <Grid item container xs={12} justify="center" alignItems="center">
                        <div className={classes.paper}>
                            <Typography component="h1" variant="h6" className={classes.title}>
                                {t('logoutConfirmMessage')}
                            </Typography>
                            <Grid item xs={12} >
                                <Grid container spacing={2} className={classes.form}>
                                    <Grid item xs={12} sm={6}>
                                        <Button
                                            onClick={handleCancel}
                                            variant="contained"
                                            fullWidth
                                        >
                                            {t('cancel')}
                                        </Button>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <Button
                                            onClick={handleSignout}
                                            variant="contained"
                                            fullWidth
                                        >
                                            {t('logout')}
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </div>
                    </Grid>
                </Grid>
            </>
        );
    }
}




export default withStyles(styles)(SignOut);