import React from "react";
import { Typography, Container, Grid, Button } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { Link } from "@reach/router";
var coreAPI = require("../../../util/coreAPI");

const HomeView = ({ classes, ...props }) => {
  const { t } = useTranslation("translations");

  function initAPI() {
    process.stdout.write("InitAPI()    called\n");
    coreAPI.InitAPI(true);
  }
  function destroyAPI() {
    process.stdout.write("Destroy called\n");
    coreAPI.DestroyAPI();
    //ipcRenderer.send('closed');
    process.stdout.write("Destroy done\n");
  }

  return (
    <React.Fragment>
      <Container>
        <Grid
          container
          style={{
            position: "absolute",
            left: "50%",
            top: "50%",
            justifyContent: "center",
            width: "90vw",
            height: "300px",
            alignItems: "center",
            display: "flex",
            transform: "translate(-50%, -50%)",
          }}
        >
          <Grid item xs={12} md={12}>
            <h1> Cloud Status = {props.appstate.cloud} </h1>
          </Grid>
          <Grid item xs={12} md={12}>
            <h1> Session Created = {props.appstate.session ? "Yes" : "No"} </h1>
          </Grid>
          <Grid item xs={12} md={12}>
            <h1> Battery = {props.appstate.battery} </h1>
          </Grid>
          <Grid item xs={12} md={12}>
            <Link to="/avatar-in">
              <Typography variant="h2">{t("mEnter")}</Typography>
            </Link>
          </Grid>
          <Grid item xs={6} md={6}>
            <Button onClick={initAPI}>
              <Typography variant="h2">{"Init"}</Typography>
            </Button>
          </Grid>
          <Grid item xs={6} md={6}>
            <Button onClick={destroyAPI}>
              <Typography variant="h2">{"Destory"}</Typography>
            </Button>
          </Grid>
        </Grid>
      </Container>
    </React.Fragment>
  );
};

export default HomeView;
