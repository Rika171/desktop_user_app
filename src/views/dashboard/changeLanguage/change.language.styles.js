import variables from "../../../styles/variables.scss";

export default theme => ({
    paper: {
        width: 556,
        // height: 460,
        // marginTop: theme.spacing(9.4),
        display: 'flex',
        flexDirection: 'column',
        boxShadow: "0 2px 10px 0 rgba(0, 0, 0, 0.16), 0 2px 5px 0 rgba(0, 0, 0, 0.26)",
        backgroundColor: variables.white,
        padding: theme.spacing(3),
        borderRadius: "16px",
        boxShadow: "3px -5px 40px 0 rgba(59, 59, 63, 0.09)",
        border: "solid 1px",
        borderColor: variables.gray2ff,
    },
    title: {
        // fontFamily: 'Archivo',
        fontWeight: 'bold',
        marginBottom: theme.spacing(1),
    },
    form: {
        width: "100%",
        marginTop: theme.spacing(5),
        justify: "center",
        alignItems: "center",
        display: 'flex',
        flexDirection: 'column',
    },
    input: {
        marginBottom: theme.spacing(5),
    },
    btnPairCon: {
        marginTop: theme.spacing(3)
    },
    body_main: {
        position: "absolute",
        top: "0",
        bottom: "0",
        left: "0",
        right: "0",
        objectFit: "contain",
        backgroundImage: "linear-gradient(to bottom, rgba(0, 102, 204, 0.07) 10%, rgba(194, 241, 255, 0.25) 47%, rgba(214, 212, 254, 0.29) 91%)"
    },
    sub_title: {
        color: variables.black666,
    }
});