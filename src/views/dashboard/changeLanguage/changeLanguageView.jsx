import React from "react";
import { Grid, CssBaseline, withStyles, Typography, Container, TextField, FormControlLabel, Checkbox, Button, Box } from "@material-ui/core";
import { withTranslation } from "react-i18next";
import LanguageSwitcher from "../../../components/common/languageSwitcher.jsx";
import { Link } from "@reach/router";
import NavigationBar from "../../../components/common/navigationBar";
import DefaultTextField from "../../../components/common/defaultTextField.jsx";
import ButtonPair from "../../../components/common/buttonPair.jsx";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import DefaultRadioGroup from "../../../components/common/defaultRadioGroup.jsx";
import styles from "./change.language.styles";

class ChangeLanguageView extends React.Component {
    render() {
        const { t, classes, i18n, formValid, errors, onChange, value } = this.props;
        // const [value, setValue] = React.useState('female');
        return (
            <>
                {/* <NavigationBar title={t('changeLanguage')} /> */}
                <Grid container component="main" className={classes.body_main}>
                    <CssBaseline />
                    <Grid item container xs={12} justify="center" alignItems="center">
                        <div className={classes.paper}>
                            <Typography component="h1" variant="h6" className={classes.title}>
                                {t('changeLanguage')}
                            </Typography>
                            <Typography component="h1" variant="caption" className={classes.sub_title}>
                                {t('changeLanguageSubtitle')}
                            </Typography>
                            <form className={classes.form} noValidate onSubmit={this.props.handleSave}>
                                <DefaultRadioGroup
                                    required
                                    options={['en', 'jp']}
                                    onChange={onChange}
                                    value={value}
                                />
                                <ButtonPair className={classes.btnPairCon} submitTitle={t('save')} cancelTitle={t("cancel")} disabled={!formValid} />
                            </form>
                        </div>
                    </Grid>
                </Grid>
            </>
        );
    }
}

export default withStyles(styles)(ChangeLanguageView);