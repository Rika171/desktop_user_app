import variables from "../../styles/variables.scss";

export default theme => ({
  body_main: {
    position: "absolute",
    top: "0",
    bottom: "0",
    left: "0",
    right: "0",
    backgroundImage: "linear-gradient(64deg, rgba(194, 241, 255, 0.44) 5%, rgba(236, 246, 252, 0.06) 48%, rgba(255, 85, 119, 0.17) 97%)"
  },
  title: {
    // fontFamily: 'Futura',
    fontSize: '1.125rem',
    fontWeight: '500',
    fontStretch: 'normal',
    fontStyle: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    color: variables.grayE6f,
    marginTop: theme.spacing(10),
    marginLeft: theme.spacing(4),
  },
});