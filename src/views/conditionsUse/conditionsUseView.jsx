import React from "react";
import { Grid, CssBaseline, withStyles, Typography, Container, TextField, FormControlLabel, Checkbox, Button } from "@material-ui/core";
import { withTranslation } from "react-i18next";
import LanguageSwitcher from "../../components/common/languageSwitcher.jsx";
import { Link } from "@reach/router";
import NavigationBar from "../../components/common/navigationBar";
import DefaultTextField from "../../components/common/defaultTextField.jsx";
import SubmitButton from "../../components/common/submitButton.jsx";
import styles from "./conditions.use.styles";

class ConditionsUseView extends React.Component {

    render() {
        const { t, classes, i18n} = this.props;

        return (
            <>
                {/* <NavigationBar title={t('conditionsOfUse')} /> */}
                <Grid container component="main" className={classes.body_main}>
                    <CssBaseline />
                    <Typography className={classes.title}>
                        {t('conditionsOfUse')}
                    </Typography>
                </Grid>
            </>
        );
    }
}

export default withStyles(styles)(ConditionsUseView);