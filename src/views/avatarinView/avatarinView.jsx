// Import dependencies
import React from "react";
import {
  Typography,
  Container,
  Grid,
  withStyles,
  Button,
  CssBaseline,
} from "@material-ui/core";
import styles from "./avatarinView.styles.js";
import VideoView from "../../components/avatar-in/videoView";
import DataView from "../../components/avatar-in/dataView";
import AudioView from "../../components/avatar-in/audioView";
import { withTranslation } from "react-i18next";
var coreAPI = require("../../util/coreAPI");
import { getUserDetailsLocal } from "../../helpers/TokenHelper";
import Slider from '@material-ui/core/Slider';

class AvatarinView extends React.Component {
  constructor(props) {
    super(props);

    this.avatarCoreDataWrapper = coreAPI.CreateDataWrapper();
  }
  componentWillUnmount() {
    this.avatarCoreDataWrapper.destroy();
    this.avatarCoreDataWrapper = null;
  }

  render() {
    const { classes, t } = this.props;

    const propsLocal = {
      resource: "local-camera-scaled",
      title: t("mLocalCamera"),
      t: this.props.t,
    };

    const propsRemoteFirstPerson = {
      resource: "remote-camera",
      title: t("mRemoteCameraFP"),
      t: this.props.t,
    };

    const propsRemoteBottomView = {
      resource: "remote-bottom-camera",
      title: t("mRemoteCameraBT"),
      t: this.props.t,
    };

    const propsDataRemote = {
      resource: "remote-data",
      channel: "robot",
      allowInput: "true",
      t: this.props.t,
    };
    const propsDataUserRemote = {
      resource: "remote-data",
      channel: "user",
      allowInput: "true",
      t: this.props.t,
    };

    return (
      <React.Fragment>
        <Grid
          container
          component="main"
          className={classes.body_main}
          style={{ width: "96vw", height: "96vh" }}
        >
          <Grid item xs={12} className={classes.grid_main}>
            <VideoView {...propsRemoteFirstPerson}></VideoView>
            <DataView {...propsDataRemote}></DataView>
            <DataView {...propsDataUserRemote}></DataView>

          </Grid>
          <Grid item xs={4} className={classes.grid_sub_right_top}>
            <VideoView {...propsLocal}></VideoView>
          </Grid>
        </Grid>
      </React.Fragment>
    );

  }
}

export default withTranslation("translations")(
  withStyles(styles)(AvatarinView)
);
