import variables from "../../styles/variables.scss";
import zIndex from "@material-ui/core/styles/zIndex";

export default theme => ({
  body_main: {
    position: "absolute",
    top: "0",
    bottom: "0",
    left: "0",
    right: "0",

  },
  grid_main: {
    height: "calc(100vh - 144px)",
    //   backgroundImage: "linear-gradient(64deg, rgba(244, 100, 255, 1) 5%, rgba(200, 246, 252, 1) 48%, rgba(255, 85, 119, 1) 97%)",
    marginTop: "64px",
    zIndex: "1"
  },
  grid_sub_left_top: {
    //   backgroundImage: "linear-gradient(64deg, rgba(194, 200, 255, 1) 5%, rgba(200, 246, 252, 1) 48%, rgba(255, 85, 119, 1) 97%)",
    zIndex: "2",
    left: "0",
    marginTop: "64px",
    position: "absolute"
  },
  grid_sub_right_top: {
    //  backgroundImage: "linear-gradient(64deg, rgba(194, 120, 255, 1) 5%, rgba(200, 246, 252, 1) 48%, rgba(120, 85, 119, 1) 97%)",
    position: "absolute",
    zIndex: "2",
    right: "0",
    marginTop: "64px"
  },
  grid_sub_left_bottom: {
    //   backgroundImage: "linear-gradient(64deg, rgba(194, 200, 255, 1) 5%, rgba(200, 246, 252, 1) 48%, rgba(255, 85, 119, 1) 97%)",
    zIndex: "2",
    left: "0",
    bottom: "0",
    position: "absolute"
  },
});