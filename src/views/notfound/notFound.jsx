import React from "react";
import {navigate} from "../../util";
import { Container, Grid, withStyles , Typography, Button } from "@material-ui/core";
import { withTranslation } from "react-i18next";


class NotFoundView extends React.Component{
    render(){
        const {classes ,t } = this.props;
        return (
            <Container className={classes.container} >
                <Grid item xs={12} className={classes.root}>
                    <Typography>Page not found.</Typography>
                    <Button className={classes.button} variant="outlined" onClick = {e =>{navigate("/")}}>
                       {t('button_back')} 
                    </Button>
                </Grid>
                
            </Container>          
        );
    }

}

const styles = theme => ({
    container: {
        width: "60%",
        margin:"auto",
        justifyContent:"center"
    },
    root:{
        display: "flex",
        flexDirection:"column",
        alignItems: "center"
    },
    button:{
        margin: theme.spacing(2)
    }
});

export default withTranslation('translations') (withStyles(styles) (NotFoundView));