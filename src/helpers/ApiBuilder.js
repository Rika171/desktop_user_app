import axios from "axios";
import {API_CONSTS} from "../_constants/api.consts";
import {setAuthToken, saveAccessToken, saveAllTokens } from "../helpers/TokenHelper";
import authService, {refreshToken} from "../_services/auth.service";

const API = axios.create({
    baseURL:API_CONSTS.API_URL,
    timeout:20000,
   // headers: {'Access-Control-Allow-Origin':'*'}
});

/**
 * Request interceptors
 */

 API.interceptors.request.use(
     request =>{
        if(request.url.includes('token_refresh')|| request.url.includes('logout_refresh')){
            request.headers.Authorization = `Bearer ${localStorage.refresh_token}`
        }
        else if(request.url.includes('login') || request.url.includes('register')){
            request.headers.Authorization = "Basic YW5hOmFuYV9zZWNyZXQ=";
        }
        else{
            request.headers.Authorization = `Bearer ${localStorage.access_token}`;
        }
        return request;
     },
     error => {
         return Promise.reject(error);
     }
 );


/**
 * Response interceptors
 */

 let fetchingAccessToken = false;
 let subscribers = []

 const onAccessTokenFetched = access_token =>{
     subscribers = subscribers.filter(callback => callback(access_token));
 }

 const addSubscriber = callback =>{
     subscribers.push(callback);
 }

 API.interceptors.response.use(
     response => response,
     error =>{
        const {config: originalRequest, response } = error;
        const status = response ? response.status : 500;
         switch(status){
             case 401:
                 {
                    if(!fetchingAccessToken){
                        fetchingAccessToken = true;
                        authService.refreshToken().then( res =>{
                            const {access_token} = res.data;
                            fetchingAccessToken = false;

                            setAuthToken(access_token);
                            saveAllTokens(res.data);
                            onAccessTokenFetched(access_token);
                        })
                        .catch(err=>{
                            console.log(err);
                        })
                        
                       
                    }

                    const retryOriginalRequest = new Promise( resolve =>{
                        addSubscriber(access_token =>{
                            originalRequest.headers.Authorization = access_token,
                            resolve(axios(originalRequest));
                        });
                    });
                    return retryOriginalRequest;
                 }
            
            default:{
                return Promise.reject(error);
            }
         }
     }
 )

 export default {
     API
 }