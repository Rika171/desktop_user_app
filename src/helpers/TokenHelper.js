import axios from "axios";

export const setAuthToken = (token) => {
  if (token) {
    axios.defaults.headers.common["Authorization"] = token;
  } else {
    delete axios.defaults.headers.common["Authorization"];
  }
};

export const saveAllTokens = (tokens) => {
  if (tokens && tokens.access_token && tokens.refresh_token) {
    localStorage.setItem("access_token", tokens.access_token);
    localStorage.setItem("refresh_token", tokens.refresh_token);
  }
};

export const clearAllTokens = () => {
  localStorage.removeItem("access_token");
  localStorage.removeItem("refesh_token");
  localStorage.removeItem("user");
};

export const saveAccessToken = (token) => {
  if (token) {
    localStorage.setItem("access_token", token);
  }
};

export const saveUserDetails = (details) => {
  if (details && details.username) {
    localStorage.setItem("user", JSON.stringify(details));
  } else {
    localStorage.setItem("user", {});
  }
};

export const getUserDetailsLocal = () => {
  const details = localStorage.getItem("user");
  return JSON.parse(details);
};

export const getAccessToken = () => {
  return localStorage.getItem("access_token");
};

export const getRefreshToken = () => {
  return localStorage.getItem("refresh_token");
};

export const hashUserName = (username) => {
  process.stdout.write(username);
  var hash = 0,
    i,
    chr;

  for (i = 0; i < username.length; i++) {
    chr = username.charCodeAt(i);
    hash = (hash << 5) - hash + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash >>> 0;
};

export const getPeerID = () => {
  return hashUserName(getUserDetailsLocal().username);
};
