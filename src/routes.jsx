import React from "react";
import { Router } from "@reach/router";
import { App } from "./app.jsx";
import LoginPage from "./pages/Login";
import NotFoundPage from "./pages/notFound";
import DashboardPage from "./pages/dashboard/main.jsx";
import TopNavBar from "./components/common/topMenuBar";
import ChangeLanguagePage from "./pages/dashboard/changeLanguage.jsx";
import AvatarinViewer from "./pages/avatarinViewer";

function PrivateRoute(props) {
    let user = localStorage.getItem("user");
    const { as: Comp } = props;
    return user ? <Comp /> : <LoginPage />;
}

function Routes() {

    return (
        <React.Fragment>
            <TopNavBar />
            <Router>
                <App path="/">
                    <PrivateRoute as={DashboardPage} path="/*" />
                    <LoginPage path="/login" />
                    <ChangeLanguagePage path="/change-lan" />
                    <AvatarinViewer path="/avatar-in-bypass" />
                    <NotFoundPage default />
                </App>
            </Router>
        </React.Fragment>
    )
}

export { Routes }