import APIBuilder from "../helpers/ApiBuilder";



const loginUser = userData =>{
    const headers = {
        "Content-Type": "application/json",
        "Authorization":"Basic YW5hOmFuYV9zZWNyZXQ="
    }
    return APIBuilder.API.post("/login", userData, headers);
}

const refreshToken = () =>{
    const headers = {
        "Content-Type": "application/json",
        "Authorization":"Basic YW5hOmFuYV9zZWNyZXQ="
    }
    let refresh_token = localStorage.getItem("refresh_token");
    const userData = {
        "grant_type":"refresh_token",
        "refresh_token": refresh_token
    }
    return APIBuilder.API.post("/login", userData, headers);
   
}

const registerUser = userData =>{
    const headers = {
        "Content-Type": "application/json",
        "Authorization":"Basic YW5hOmFuYV9zZWNyZXQ="
    }
    return APIBuilder.API.post("/register", userData, headers);
}

/**
 * Later this api call must be integrated to login 
 */

const getUserDetails = () => {
    return APIBuilder.API.get("/users/info");
}

const logoutUser = token =>{
    return  APIBuilder.API.post("/logout",token);
}

export default{
    loginUser,
    logoutUser,
    registerUser,
    getUserDetails,
    refreshToken
}