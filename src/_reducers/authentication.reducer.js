import { USER_CONSTS } from "../_constants";
import isEmpty from "is-empty";

let user = JSON.parse(localStorage.getItem("user"));

const initialState =  user ? {
    isAuthenticated: true,
    user: user,
    loading:false

} : {    isAuthenticated : false,
    user: {},
    loading: false
};

export const authentication = (state = initialState, action) => {
    switch (action.type) {
        case USER_CONSTS.LOGIN_REQUEST:
            return {
                ...state,
                isAuthenticated: false,
                user: {}
            }
            break;
        case USER_CONSTS.LOGIN_SUCCESS:
            console.log("login success reducer", action.user);
            return {
                ...state,
                isAuthenticated: true,
                user: action.user
            }
            break;

        case USER_CONSTS.LOGOUT_SUCCESS:
            return {
                ...state,
                isAuthenticated: false,
                user: {}
            }
            break;

        case USER_CONSTS.LOGOUT_ERROR:
            return {
                ...state,
                isAuthenticated: false,
                user:{}
            }
            break;

        default:
            return state;
    }

}