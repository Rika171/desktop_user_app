// src/index.jsx

// Import dependencies
import React, { Suspense } from "react";
import { render } from 'react-dom'
import { LocationProvider } from "@reach/router";
import { Routes } from "./routes.jsx";
import { MuiThemeProvider } from "@material-ui/core";
import { history, StoreProvider } from "./util";
import { I18nextProvider } from "react-i18next";
import i18n from "../i18n/i18n";
import theme from "./styles/theme";


// Since we are using HtmlWebpackPlugin WITHOUT a template, we should create our own root node in the body element before rendering into it
let root = document.createElement('div')

// Append root div to body
root.id = 'root'
document.body.appendChild(root)

const Loader = () => {
    return (<div>Loading ...</div>)
}

render(
    <StoreProvider>
     <LocationProvider history={history}>
        <Suspense fallback={<Loader />}>
            <I18nextProvider i18n={i18n}>
               <MuiThemeProvider theme={theme}>
                   
                        <Routes />
                    
               </MuiThemeProvider> 
            </I18nextProvider>
        </Suspense>
    </LocationProvider>       
    </StoreProvider>

    , document.getElementById("root"))