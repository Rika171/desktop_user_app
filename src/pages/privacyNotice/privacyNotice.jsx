import React from "./node_modules/react";
import PrivacyNoticeView from "../../views/privacyNotice";
import { connect } from "./node_modules/react-redux";
import { withTranslation } from "./node_modules/react-i18next";

class PrivacyNoticePage extends React.Component{
    constructor(props){
        super(props);
    }

    render() {
        return <PrivacyNoticeView {...this.props} />;
    }
}

export default connect(null)(withTranslation('translations') (PrivacyNoticePage));