import React from "react";
import {navigate} from "../../util";
import {signOut} from "../../_actions/user.action";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";
import SignoutView from "../../views/dashboard/signout";

class SignOut extends React.Component{
    constructor(props){
        super(props);
    }

    handleOnCancel = e =>{
        navigate("/");
    }

    handleOnSignout = e =>{
        this.props.signOut();
    }

    render(){
        const props = {
            ...this.props,
            handleCancel : this.handleOnCancel,
            handleSignout: this.handleOnSignout
        }
        return <React.Fragment>
            <SignoutView {...props} />

        </React.Fragment>;
    }
}

const mapStateToProps = state =>{
   
    return {
       auth: state.auth
    };
}

export default  connect(mapStateToProps, {signOut})( withTranslation('translations') (SignOut));