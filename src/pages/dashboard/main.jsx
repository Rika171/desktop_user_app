import React from "react";
import { Router } from "@reach/router";
import AvatarinViewer from "../avatarinViewer";
import Home from "./home.jsx";
import NotFound from "../notFound";

import Footer from "../../components/common/footer";
import { Grid } from "@material-ui/core";
import SignOutPage from "./signout.jsx";
import ChangeLanguagePage from "./changeLanguage.jsx";

const Main = () => {
  return (
    <React.Fragment>
      <Grid container>
        <Router>
          <Home path="/*" />
          <SignOutPage path="/signout" />
          <AvatarinViewer path="/avatar-in" />
          <ChangeLanguagePage path="/change-lan" />
          <NotFound default />
        </Router>
      </Grid>
    </React.Fragment>
  );
};

export default Main;
