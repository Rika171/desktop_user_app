import React from "react";
import ChangeLanguageView from "../../views/dashboard/changeLanguage";

import { connect } from "react-redux";
import { withTranslation } from "react-i18next";
import { navigate } from "@reach/router";

class ChangeLanguagePage extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            lan: localStorage.getItem("lang") || this.props.i18n.language,
            lanValid: false,
            formValid: false,
            errors: {}
        }
    }

    onChange = e =>{
        let {id , value} = e.target;
        this.setState({
            lan: value,
            formValid: value.length > 0
        });
    }

    handleSave = e => {
        e.preventDefault();
        this.props.i18n.changeLanguage(this.state.lan);
        const changeLanData = {
            languageCode: this.state.lan,
        }
        console.log("changeLanData >>>", changeLanData);
        //this.props.updateProfile(changeLanData);
        navigate(-1);
    }

    render(){
        const {lan, formValid, errors} = this.state;
        const props = {
            ...this.props,
            value: this.state.lan,
            onChange: this.onChange,
            handleSave: this.handleSave,
            formValid,
            errors
        }
        return <ChangeLanguageView  {...props}/>;
    }
}

const mapStateToProps = state =>({
    sprof : state.profile
});

export default connect(mapStateToProps)(withTranslation('translations') (ChangeLanguagePage));