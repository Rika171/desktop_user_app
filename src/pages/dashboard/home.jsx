import React from "react";
import HomeView from "../../views/dashboard/home";
var coreAPI = require("../../util/coreAPI");
import AuthService from "../../_services/auth.service";
import {
  getUserDetailsLocal,
  getAccessToken,
  getPeerID,
} from "../../helpers/TokenHelper";

import { navigate } from "../../util";

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = { cloud: "offline", session: false, battery: "" };
    this.subscribed = false;
  }

  onApplicationStart(service, path, data) {
    console.log("onApplicationStart");
    /*
        var settings = {
          peerIP: data,
        };
        coreAPI.PublishMessage(
          "RTP/sender/.*",
          "service/setsettings",
          JSON.stringify(settings)
        );*/

    navigate("/avatar-in");
  }

  onCoreInited() {

    coreAPI.SetAppConfigurationFilePath("conf/app.conf",true)
    coreAPI.SubscribeToMessage(
      "application",
      "cloud/status",
      (name, path, data) => {
        this.setState({ cloud: data });
        console.log("Cloud is: " + data);
      }
    );
    coreAPI.SubscribeToMessage("application", "cloud/peer/joining", () => {
      this.setState({ cloud: "Peer is joining" });
    });
    coreAPI.SubscribeToMessage("application", "cloud/session/created", () => {
      this.setState({ session: true });
    });
    coreAPI.SubscribeToMessage(
      "application",
      "botshell/battery",
      (service, path, data) => {
        console.log("Battery arrived: " + service + ", " + path + ", " + data);
        this.setState({ battery: data });
      }
    );
    coreAPI.SubscribeToMessage(
      "application",
      "cloud/peer/joined",
      this.onApplicationStart.bind(this)
    );

    setTimeout(() => {
      var peerIDObj = {
        peerID: this.peerID.toString(),
      };
      coreAPI.PublishMessage("", "cloud/peer/set", JSON.stringify(peerIDObj));
      coreAPI.PublishMessage("", "cloud/status/get", "");
    }, 1000);
  }

  componentDidMount() {
    this.peerID = getPeerID();
    this.setState({ cloud: "offline" });
    if (coreAPI.IsCoreInited()) this.onCoreInited();
    coreAPI.AddCallbackFunction(
      coreAPI.DefaultCallbacks.InitCallbackType,
      () => {
        this.onCoreInited();
      }
    );
  }
  componentWillUnmount() {
    coreAPI.UnsubscribeToMessage("application", "application/start");

    coreAPI.UnsubscribeToMessage("application", "cloud/status");
    coreAPI.UnsubscribeToMessage("application", "cloud/peer/joining");
    coreAPI.UnsubscribeToMessage("application", "cloud/peer/joined");
    coreAPI.UnsubscribeToMessage("application", "cloud/session/created");
    coreAPI.UnsubscribeToMessage("application", "botshell/battery");
  }

  render() {
    return (
      <React.Fragment>
        <HomeView appstate={this.state} />
      </React.Fragment>
    );
  }
}

export default Home;
