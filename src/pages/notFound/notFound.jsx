import React from "react";
import NotFoundView from "../../views/notfound";

class NotFound extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return <NotFoundView/>;
    }
}

export default NotFound;