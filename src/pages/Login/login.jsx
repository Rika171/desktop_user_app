import React from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import LoginView from "../../views/login/loginView.jsx";
import { withTranslation } from 'react-i18next';
import {signIn} from "../../_actions/user.action";
import { Redirect } from "@reach/router";


class LoginPage extends React.Component {

    constructor(props) {
        super(props);
        this.state ={
            email: "",
            password: "",
            userCheck: false,
            showPassword:false,
            emailValid: false,
            passwordValid: false,
            userCheckValid: false,
            formValid:false,
            redirect: false,
            errors: {}
        }
    }

    componentDidMount(){

        if(this.props.auth.isAuthenticated){
            this.setState({redirect:true});
        }
    }

    onChange = e =>{
        let {id, value} = e.target;
        if(id === 'userCheck') {
            this.setState({userCheck: !this.state.userCheck}, ()=>{
                this.validateField('userCheck', this.state.userCheck)
            });
        }else {
            this.setState({[id]: value}, ()=>{
                this.validateField(id, value)
            });
        }
    }

    handleSignIn = e =>{
        e.preventDefault();
        const userData = {
            username : this.state.email,
            password : this.state.password,
            //userCheck : this.state.userCheck,
            grant_type: "password"
        }

        this.props.signIn(userData);

    }

    validateField = (name, value)=>{
        let {emailValid, passwordValid, userCheckValid, errors} = this.state;
        switch(name){
            case "email":
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                errors.email = emailValid? "": "error_email_invalid";
                break;
            case "password":
                passwordValid = value.length > 0;
                errors.password = passwordValid ? "": "error_password_blank";
                break;
            case "userCheck":
                userCheckValid = value;
                errors.userCheck = userCheckValid ? "": "error_user_check_blank";
                break;
            default:
                break;
        }
        this.setState({
            errors,
            emailValid,
            passwordValid,
            userCheckValid,
        }, this.validateForm);
    }

    validateForm = ()=>{
        this.setState({
            formValid: this.state.emailValid && this.state.passwordValid && this.state.userCheckValid
        }           
        );
    }


    render() {
        const {redirect, formValid, errors} = this.state;
        if(redirect){
            return <Redirect to="/" />
        }
        else{

            const props = {
                handleSignIn : this.handleSignIn,
                onChange : this.onChange, 
                formValid,
                errors
           }
            return <LoginView {...props} useSuspense={false} />; 
        }
 
    }

}

LoginPage.propTypes = {
    signIn : PropTypes.func.isRequired,
    auth : PropTypes.object.isRequired
};

const mapStateToProps = state =>{ 
    return {
       auth: state.auth
    };
}

export default connect(mapStateToProps, {signIn})(LoginPage);