import React from "react";
import AvatarinView from "../../views/avatarinView";
var coreAPI = require("../../util/coreAPI");
import { withTranslation } from "react-i18next";
import { navigate } from "../../util";
import { hashUserName, getPeerID } from "../../helpers/TokenHelper";

class AvatarinViewer extends React.Component {
  constructor(props) {
    super(props);
    this.canvasRef = React.createRef();
    this.targetPeerID = "4003";
  }
  onApplicationStop(service, path, data) {
    console.log("onApplicationStart");

    navigate("/*");
  }

  onFacialLandmarks(service, path, data) {
    process.stdout.write(data);
  }

  componentDidMount() {
    console.log("AvatarinViewer->componentDidMount");

    setTimeout(function () {
      var peerID = getPeerID();
      //example for setting application configuration value
      coreAPI.SetAppConfiguation("PeerID",peerID.toString());
      
      var remotePeerID = hashUserName("mrayyamen88@gmail.com").toString();
      if (peerID != remotePeerID) //ask the cloud to connect to a remote peer
        coreAPI.PublishMessage("", "cloud/joinpeer", remotePeerID);

      //Start services either by name, or using wildcart 
      coreAPI.StartService("RTP/proxy");
      coreAPI.StartService("Capture/.*");
      coreAPI.StartService("RTP/receiver/.*");
      
      this.sender = true;
      if (this.sender == true) 
        coreAPI.StartService("RTP/sender/.*");

    }, 1000);

    //subscribe to peer diconnected message
    coreAPI.SubscribeToMessage(
      "application",
      "cloud/peer/disconnected",
      this.onApplicationStop.bind(this)
    );
  }

  componentWillUnmount() {
    console.log("AvatarinViewer->componentWillUnmount");

    //stop running services
    coreAPI.StopService("Capture/.*");
    coreAPI.StopService("RTP/.*");

    // tell the remote peer that we are disconnecting
    var remotePeerID = hashUserName("mrayyamen88@gmail.com").toString();
    coreAPI.PublishMessage("", "cloud/disconnectpeer", remotePeerID);
    coreAPI.UnsubscribeToMessage("application", "cloud/peer/disconnected");
  }

  render() {
    const props = {
      ...this.props,
    };
    return <AvatarinView {...props} />;
  }
}

export default withTranslation("translations")(AvatarinViewer);
